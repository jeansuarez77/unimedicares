<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="author" content="Theme Starz">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.css')}}" type="text/css">
      <link rel="stylesheet" href="{{asset('assets/css/selectize.css')}}" type="text/css">
      <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}" type="text/css">
      <link rel="stylesheet" href="{{asset('assets/css/vanillabox/vanillabox.css')}}" type="text/css">
      <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" type="text/css">
      <link rel="stylesheet" href="{{asset('assets/css/toastr.min.css')}}" type="text/css">

      <script src="{{ asset('js/app.js') }}" defer></script>
      <title>Unimedicares</title>
      @laravelPWA

   </head>
   <body  class="page-sub-page page-microsite">
      <!-- Wrapper -->
      <div id="app" class="wrapper">
         <!-- Header -->
         <div class="navigation-wrapper">
            
            <!-- /.secondary-navigation -->
            <div class="secondary-navigation-wrapper" style="background-color:#0D366D !important">
                <div class="container">
                    <aside  class="logo" style="text-align: center; ">
                     <img style="max-width: 350px;" src="{{asset('assets/img/logos/logo.png')}}" width="300" class="">
                    </aside>
                </div>
            </div>
            <div class="primary-navigation-wrapper">
               <header class="navbar" id="top" role="banner">
                  <div class="container">
                     
                     <!-- menu mobil -->
                     <div class="col-md-12 nav-mobil">

                        <ul class="nav navbar-nav menu-mobil" >
                           <li class="{{Request::is('/')?'active':''}}">
                              <a href="{{url('/')}}">Inicio</a>
                           </li>

                           <li class="{{Request::is('programas','programas/*')?'active':''}}">
                              <a href="{{route('courses.index')}}">Capacítate</a>
                           </li>
                           
                           @if(Auth::guest())
                             
                                 <li>
                                    <a href="{{ route('login') }}">Iniciar</a>
                                 </li>
                           @endif

                           @if(!Auth::guest())
                              <li class="{{Request::is('perfil')?'active':''}}">
                                 <a href="{{route('students.profile')}}">Mis cursos</a>
                              </li>
                           
                           @endif
                           @if(!Auth::guest() &&  Auth::user()->rol != 'admin')
                              <li>
                                 <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    Salir
                                 </a>

                                 <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>

                              </li>

                           @endif
                           @if(!Auth::guest() &&  Auth::user()->rol = 'admin')

                           <li style="border: 0px solid white !important; width: 15px;">
                              <div class="navbar-header" style="width: 45px;">
                                 <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                                     <span class="	fa fa-bars"></span>
                                 </button>
                                 
                             </div>
                           </li>

                           @endif

                        </ul>

                        
                        


                     </div>
                     
                     <div class="col-md-12">

                       

                        <nav class="collapse navbar-collapse bs-navbar-collapse navbar-center col-md-9 col-md-offset-2" role="navigation">
                           <ul class="nav navbar-nav">
                              <li class="{{Request::is('/')?'active':''}}">
                                 <a href="{{url('/')}}">Inicio</a>
                              </li>
                              
                              <li class="{{Request::is('programas','programas/*')?'active':''}}">
                                 <a href="{{route('courses.index')}}">Capacítate</a>
                              </li>
                              
                              <li class="{{Request::is('perfil')?'active':''}}" ><a href="{{route('students.profile')}}">Mis cursos</a></li>
                              @if(!Auth::guest() &&  Auth::user()->rol = 'admin')
                                 <li class="{{Request::is('admin','admin/*')?'active':''}}">
                                    <a href="#" class="has-child no-link">Administración</a>
                                    <ul class="list-unstyled child-navigation">
                                       <li><a href="{{url('admin/courses')}}">Gestionar Cursos</a></li>
                                       <li><a href="{{url('admin/courses/areas')}}">Gestionar areas</a></li>
                                       <li><a href="{{url('admin/students')}}">Gestionar estudiantes</a></li>
                                       <li><a href="{{url('admin/questions')}}">Banco de preguntas</a></li>
                                       <li><a href="{{url('admin/users')}}">Usuarios</a></li>
                                    </ul>
                                 </li>
                              @endif

                              @if(!Auth::guest())

                                 <li class="{{Request::is('students','students/*')?'active':''}}">
                                    <a href="{{route('students.profile')}}"> <i class="fa fa-user"></i> Perfil</a>
                                 </li>

                                 <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                       Salir
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                       @csrf
                                 </form>

                                 </li>

                                 
                              @endif

                              @if(Auth::guest())
                                 <li>
                                    <a href="{{ route('register') }}">Registrate</a>
                                 </li>
                              @endif

                              @if(Auth::guest())
                                 <li>
                                    <a href="{{ route('login') }}">Iniciar sesión</a>
                                 </li>
                              @endif

                           </ul>
                        </nav>

                     </div>
                     
                     <!-- /.navbar collapse-->
                     <div class="social">
                        <div class="icons">
                           
                        </div>

                        
                        <!-- /.icons -->
                     </div>
                  </div>
                  <!-- /.container -->
               </header>
               <!-- /.navbar -->
            </div>
            <!-- /.primary-navigation -->
         </div>
         <!-- end Header -->
         <div id="page-content">
            <!-- Slider -->

            @yield('content')
           
            <!-- end Content -->
         </div>
         <!-- Footer -->
         <footer id="page-footer">
            
            <!-- /#footer-top -->
            <section class="" id="footer-content">
               <div class="container">
                  <div class="row">
                     <div class=" hidden-xs col-md-3 col-sm-12">
                        <aside class="logo">
                           <img style="max-width: 350px;" src="{{asset('assets/img/logos/logo.png')}}" style="width:-webkit-fill-available" class="vertical-center">
                        </aside>
                     </div>
                     <!-- /.col-md-3 -->
                     
                     <!-- /.col-md-3 -->
                  </div>
                  <!-- /.row -->
               </div>
               <!-- /.container -->
               <div class="background"><img src="{{asset('assets/img/background-city.png')}}" class="" alt=""></div>
            </section>
            <!-- /#footer-content -->
            <section id="footer-bottom">
               <div class="container">
                  <div class="footer-inner">
                     <div class="copyright">© Todos los derechos reservados</div>
                     <!-- /.copyright -->
                  </div>
                  <!-- /.footer-inner -->
               </div>
               <!-- /.container -->
            </section>
            <!-- /#footer-bottom -->
         </footer>
         <!-- end Footer -->
      </div>
      <!-- end Wrapper -->
      <script type="text/javascript" src="{{asset('assets/js/jquery-2.1.0.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('assets/js/jquery-migrate-1.2.1.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('assets/js/selectize.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('assets/js/jquery.validate.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('assets/js/jquery.placeholder.js')}}"></script>
      <script type="text/javascript" src="{{asset('assets/js/jQuery.equalHeights.js')}}"></script>
      <script type="text/javascript" src="{{asset('assets/js/icheck.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('assets/js/jquery.vanillabox-0.1.5.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('assets/js/retina-1.1.0.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('assets/js/toastr.min.js')}}"></script>
      <script src='https://johnny.github.io/jquery-sortable/js/jquery-sortable.js'></script>
      <script type="text/javascript" src="{{asset('assets/js/sweetalert.min.js')}}"></script>

      @yield('js')
   </body>
</html>