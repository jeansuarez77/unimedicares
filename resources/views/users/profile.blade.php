@extends('layouts.app')

@section('content')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}">Inicio</a></li>
        <li class="active">Mi cuenta</li>
    </ol>
</div>


<div class="container">
    <header><h2>Mi cuenta</h2></header>
    <div class="row">
        <div class="col-md-12">
            <section id="my-account">
                <ul class="nav nav-tabs" id="tabs">
                    <li class="active"><a href="#tab-courses" data-toggle="tab" aria-expanded="true">Mis Cursos</a></li>
                    <li class=""><a id="tab-profile-a" href="#tab-profile" data-toggle="tab" aria-expanded="false">Perfil</a></li>
                    <li class=""><a href="#tab-change-password" data-toggle="tab" aria-expanded="false">Cambiar contraseña</a></li>
                </ul><!-- /#my-profile-tabs -->
                <div class="tab-content my-account-tab-content">

                    <div class="tab-pane panel active col-md-12" id="tab-courses">
                        <section id="course-list">
                            <header><h3  class="text-black">Mis Cursos</h3></header>
                            <div class="row">
                                @foreach ($courses as $item)
                                    <div class="col-xs-12 col-md-3  text-center" style="margin-bottom: 10px;">
                                        <div class="card-course">                                    
                                            <a href="/programas/curso/{{$item->url}}">
                                                <img src="{{$item->logo}}" class="logo-img" style="height: 120px;-webkit-border-radius: 50%; background-color: #0d366d;" >
                                                <h6  class="text-white"><b style="color: #000;">{{$item->name}}</b></h6>
                                                
                                            </a>
                                            <div class="course-progress">
                                                <div class="progress" style="background-color:#d5d5d5; border-radius: 20px">
                                                    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="progress-bar text-center" style="width: {{(int)$item->progress}}%;"> {{(int)$item->progress}}%</div>
                                                </div>
                                            </div>
                                            
                                            @if ($item->state == 0 and $item->approved == 1)
                                                    @if(!$is_data_valid)
                                                        <a type="button" id="btn-error" title="Generar certificado" class="btn  btn-small"><i class="fa fa-file-pdf-o"></i> Imprimir constancia</a>
                                                    @elseif($item->is_free || $item->is_payment_finished == 1 )
                                                        <a href="{{route('students.certificate',['student_id' => $user_id, 'course_id' => $item->id])}}" title="Generar constancia" class="btn  btn-small"><i class="fa fa-file-pdf-o"> Imprimir constancia</i></a>
                                                    
                                                    @else
                                                        <a href="javascript:checkouts_pay('{{$item->id}}')" class=" btn  btn-small" title="Pagar Certificado"><i class="fa fa-credit-card"></i> Pagar comprobante</a>
                                                    @endif

                                                @else
                                                <a type="button" disabled title="Generar constancia" class="btn  btn-small"><i class="fa fa-file-pdf-o"> Imprimir constancia</i></a>
                                            @endif

                                        </div>
                                        
                                    </div>
                                @endforeach
                            </div>
                            <!--<div class="table-responsive">
                                <table class="table table-hover table-responsive course-list-table tablesorter">
                                    <thead>
                                    <div class="form-group">
                                        <th class="header">Nombre</th>
                                        <th class="header">Area</th>
                                        <th class="starts header">Estado</th>
                                        <th class="starts header">Certificado</th>
                                        <th class="starts header">Precio del curso</th>
                                        <th class="status header">Fecha registro</th>
                                    </div>
                                    </thead>
                                    <tbody>

                                        @foreach ($courses as $item)
                                            <tr class="status-not-started">
                                                <th class="course-title"><a href="{{route('courses.course',['url'=>$item->url])}}">{{$item->name}}</a></th>
                                                <th class="course-category">{{$item->name}}</th>
                                                <th>
                                                    {{$item->state? 'En curso': 'Finalizado'}}
                                                </th>
                                                <th>
                                                    @if ($item->state == 0 and $item->approved == 1)
                                                        @if(!$is_data_valid)
                                                            <a type="button" id="btn-error" title="Generar certificado" class="btn  btn-small"><i class="fa fa-file-pdf-o"></i></a>
                                                        @elseif($item->is_free || $item->is_payment_finished == 1 )
                                                            <a href="{{route('students.certificate',['student_id' => $user_id, 'course_id' => $item->id])}}" title="Generar certificado" class="btn  btn-small"><i class="fa fa-file-pdf-o"></i></a>
                                                        
                                                        @else
                                                            <a href="javascript:checkouts_pay('{{$item->id}}')" class=" btn  btn-small" title="Pagar Certificado"><i class="fa fa-credit-card"></i></a>
                                                        @endif

                                                    @endif
                                                </th>
                                                <th>${{ $item->purchase_price}}</th>
                                                <th class="status"><i class="fa fa-calendar-o"></i>{{ date("d-M-Y", strtotime($item->date_start)) }}</th>
                                            </div>
                                        @endforeach

                                        <?php if($courses->count() == 0) { ?>
                                            <tr style="cursor: pointer;"><td colspan="5"><span class="col-md-12 text-center text-warning">No hay registro que mostrar</span></td></tr>
                                        <?php } ?>
                                        

                                    </tbody>
                                </table>
                            </div>-->
                          
                        </section><!-- /#course-list -->
                    </div><!-- /.tab-pane -->

                    <div class="tab-pane panel col-md-12" id="tab-profile">
                        <section id="my-profile">
                            <header><h3 class="text-black">Mis Datos</h3></header>
                            <div class="row">
                                
                                <div class="col-md-5 col-md-offset-4" style="min-height: 0px;">
                                    <form id="form_info" class="clearfix" action="{{route('students.update-info')}}" method="post" >
                                        @csrf  

                                                <div class="form-group">
                                                    <label class="title text-black">Docuemento: </label>
                                                    
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" disabled value="{{ $user->code_doc }}: {{$user->document}}">
                                                        </div><!-- /input-group -->
                                                </div>

                                                <div class="form-group">
                                                    <label class="title text-black">Lugar de expedición documento</label>
                                                    
                                                        <div class="input-group">

                                                            <select id="document_city_id" name="document_city_id" class="form-control text-black" style="width: 100% !important">
                                                        
                                                                <option value="" disabled selected> Seleccione</option>
                                                                @foreach ($cities as $item)
                                                                <option value="{{$item->id}}">{{$item->name}} - {{$item->name_d}} - {{$item->country}}</option>
                                                                @endforeach
                                                            </select>

                                                        </div><!-- /input-group -->
                                                </div>

                                                <div class="form-group">
                                                    <label class="title text-black">Nombres: </label>
                                                    
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" required name="name" id="name" value="{{$user->name}}">
                                                        </div><!-- /input-group -->
                                                </div>

                                                <div class="form-group">
                                                    <label class="title text-black">Primer apellido: </label>
                                                    
                                                        <div class="input-group">
                                                            <input type="text" name="surname"  required class="form-control" id="surname" value="{{$user->surname}}">
                                                        </div><!-- /input-group -->
                                                </div>

                                                <div class="form-group">
                                                    <label class="title text-black">Segundo Apellido: </label>
                                                    
                                                        <div class="input-group">
                                                            <input type="text" name="second_surname" required class="form-control" id="second_surname" value="{{$user->second_surname}}">
                                                        </div><!-- /input-group -->
                                                </div>

                                                <div class="form-group">
                                                    <label class="title text-black">Correo Electronico: </label>
                                                    
                                                        <div class="input-group">
                                                            <input type="text"  class="form-control" required disabled value="{{ $user->email }} ">
                                                        </div><!-- /input-group -->
                                                </div>

                                                <div class="form-group">
                                                    <label class="title text-black">Ciudad de residencia: </label>
                                                    
                                                        <div class="input-group">

                                                            <select id="city_id" name="city_id" required class="form-control text-black" style="width: 95% !important">
                                                        
                                                                <option value="" disabled selected> Seleccione</option>
                                                                @foreach ($cities as $item)
                                                                <option value="{{$item->id}}">{{$item->name}} - {{$item->name_d}} - {{$item->country}}</option>
                                                                @endforeach
                                                            </select>

                                                        </div><!-- /input-group -->
                                                </div>

                                                <div class="form-group">
                                                    <label class="title text-black">Telefono: </label>
                                                    
                                                        <div class="input-group">
                                                            <input type="number" name="phone" required class="form-control" id="phone" value="{{$user->phone}}">
                                                        </div><!-- /input-group -->
                                                </div>

                                                <div class="form-group">
                                                    <label class="title text-black">Dirección: </label>
                                                    
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" required name="address" id="address" value="{{$user->address}}">
                                                        </div><!-- /input-group -->
                                                </div>
                                            
                                        <button type="submit" id="btn-save-info" class="btn pull-right">Guardar Cambios</button>
                                    </form>

                                </div>
                            </div><!-- /.my-profile -->
                        </section><!-- /#my-profile -->
                    </div><!-- /tab-pane -->
                    
                    <div class="tab-pane panel col-md-12 text-black" id="tab-change-password">
                        <section id="password">
                            <header><h3>Cambio de contraseña</h3></header>
                            <div class="row">
                                <div class="col-md-5 col-md-offset-4" style="min-height: 0px;">
                                  
                                    <form id="form_pass" class="clearfix" action="{{route('students.change-password')}}" method="post" >
                                    @csrf    
                                    
                                        <div class="form-group">
                                            <div style="display:none" id="alert" class="alert alert-danger" role="alert">
                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="current-password">Contraseña actual: </label>
                                            <input class="form-control" value="{{ old('password_current') }}" required type="password" name="password_current" id="current-password">

                                            @error('password')
                                                <label for="password_current" class="error">{{$message}}</label>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="new-password">Nueva Contraseña: </label>
                                            <input value="{{ old('password') }}" required type="password" name="password" class="form-control" id="new-password">
                                            @error('password')
                                                <label for="password" class="error">{{$message}}</label>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="repeat-new-password">Repita Contraseña: </label>
                                            <input  value="{{ old('password_confirmation') }}" required type="password" name="password_confirmation" class="form-control" id="repeat-new-password">
                                            @error('password')
                                                <label for="password_confirmation" class="error">{{$message}}</label>
                                            @enderror
                                        </div>
                                        <button type="submit" id="btn-save" class="btn pull-right">Cambiar contraseña: </button>
                                    </form>
                                </div>
                            </div>
                        </section>
                    </div>
                </div><!-- /.tab-content -->
            </section>

            <form method="post" id="form-pay-course"  action="https://checkout.payulatam.com/ppp-web-gateway-payu/">
                <input name="merchantId" id="merchantId"     type="hidden"  value=""   >
                <input name="accountId"  id="accountId"     type="hidden"  value="" >
                <input name="description"  id="description"   type="hidden"  value=""  >
                <input name="referenceCode" id="referenceCode"  type="hidden"  value="" >
                <input name="amount"   id="amount"       type="hidden"  value=""   >
                <input name="tax"   id="tax"          type="hidden"  value="0"  >
                <input name="taxReturnBase"  id="taxReturnBase" type="hidden"  value="0" >
                <input name="currency"   id="currency"     type="hidden"  value="COP" >
                <input name="signature"  id="signature"     type="hidden"  value=""  >
                <input name="test"     id="test"       type="hidden"  value="0" >
                <input name="buyerEmail" id="buyerEmail"     type="hidden"  value="" >
                <input name="responseUrl"  id="responseUrl"   type="hidden"  value="" >
                <input name="confirmationUrl" id="confirmationUrl" type="hidden"  value="" >
            </form>

        </div>




    </div><!-- /.row -->
</div><!-- /.container -->

@endsection

@section('js')

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>


<script>
    jQuery(document).ready(function() {

        $("#city_id").select2({
            placeholder: "Seleccione ciudad de residencia",
        });
        $("#document_city_id").select2({
            placeholder: "Seleccione ciudad de residencia",
        });

        $('#city_id').val({!! json_encode($user->city_id) !!}).trigger('change');
        $('#document_city_id').val({!! json_encode($user->document_city_id) !!}).trigger('change');

        $("#alert").hide();
        $("#btn-error").click(function(e){
            toastr.error('Debes completar los datos personales antes de generar el certificado', 'error');
            $("#tab-profile-a").click();
        });

        $("#form_pass").on('submit', function(evt){
            evt.preventDefault();  
            var input = $("#form_pass").serialize();
            let url = $('#form_pass').attr('action');
            $("#btn-save").prop( "disabled", true );
            axios.post(`${url}`,input).then(resp => {   
                $("#btn-save").prop( "disabled", false );                  
           
                    if(resp.data.success){                        
                        toastr.success('Registro guardado', 'exito');  
                        $('#form_pass')[0].reset();
                                                      
                    }else{
                        toastr.error('Error al guardar', 'error');
                    }
                }).catch(error => {
                    $("#alert").show();
                    $("#alert").html("");
                    if(error.response.status == 422){
                        let errors = error.response.data.errors;
                        let er = [];
                        if(errors != null){
                            for (const key in errors) {
                                let error = errors[key];
                                for (const key2 in error) {
                                    er.push(error[key2]);
                                }
                            }
                        }
                        show_errors(er) ;   
                        $("#btn-save").prop( "disabled", false );                  
                    }
                });
            
        });


        // send update info user

        $("#form_info").on('submit', function(evt){
            evt.preventDefault();  
            var input = $("#form_info").serialize();
            let url = $('#form_info').attr('action');
            $("#btn-save-info").prop( "disabled", true );
            axios.post(`${url}`,input).then(resp => {   
                $("#btn-save-info").prop( "disabled", false );                  
            
                    if(resp.data.success){                        
                        toastr.success('Registro guardado', 'exito');  
                                                        
                    }else{
                        toastr.error('Error al guardar', 'error');
                    }

                }).catch(error => {

                    $("#btn-save-info").prop( "disabled", false ); 

                    let errors = error.response.data.errors;

                    if(errors != null){
                        for (const key in errors) {
                            let error = errors[key];
                            for (const key2 in error) {
                                toastr.error(error[key2], 'error');
                                return false;
                            }
                        }
                    }

                    toastr.error('Error al guardar', 'error');


                });
            
        });
    });

    function checkouts_pay(id_curso) {
        

        $.get( "{{url('students/checkouts')}}/"+id_curso,{}, function( data ) {
            
            $("#accountId").val(data.accountId);
            $("#amount").val(data.amount);
            $("#buyerEmail").val(data.buyerEmail);
            $("#merchantId").val(data.merchantId);
            $("#referenceCode").val(data.referenceCode);
            $("#responseUrl").val(data.responseUrl);
            $("#signature").val(data.signature);
            $("#description").val(data.description);


            $("#form-pay-course").submit();
        });
    }

    function show_errors(erros){
        if(erros.length > 0){
            $("#alert").show();
            $("#alert").html("");

            for (const key in erros) {
                $("#alert").append(`<p style="color: #202020!important;">*${erros[key]}</p>`);
            }
        }
        setTimeout(()=>{
                $("#alert").hide();
        },8000);
    }

   

</script>

@endsection
