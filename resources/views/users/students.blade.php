@extends('layouts.app')

@section('content')
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Estudiantes</li>
        </ol>
        <hr>
        <section id="course-list">
            <div class="panel panel-info">
            <div class="panel-heading">Estudiantes <span class="label label-primary" title="{{$students->total()}} estudiantes en total">{{$students->total()}}</span></div>
                <div class="panel-body">
                    <div class="row">
                        
                        <form action="{{url('admin/students')}}" method="get" autocomplete="off" role="form" class="form-inline">
                            <div class="col-md-5">
                                <div class="input-group">
                                    <input name="search" value="{{$search}}" type="text" class="form-control" placeholder="Ingresar documento, correo o nombre">
                                    <span class="input-group-btn">                                    
                                        <button type="submit" class="btn pull-right"><i class="fa fa-search"></i></button>
                                    </span>                               
                                </div>
                            </div>
                          
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover course-list-table tablesorter table-sm">
                            <thead>
                            <tr>
                                <th  class="starts">#</th>
                                <th class="starts">Documento</th>
                                <th class="starts">Nombre</th>
                                <th class="starts">Correo</th>
                                <!--<th class="starts">Opciones</th>-->
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($students as $student) {?>
                                
                                    <tr>
                                        <th>{{$student->id}}</th>
                                        <th><?= "{$student->document}" ?></th>
                                        <th><?= "{$student->name} {$student->surname} {$student->second_surname}" ?></th>
                                        <th>{{$student->email}}</th>
                                        <!--<th>
                                        <a href="/" class="btn  btn-small" title="Actualizar Artículo"><i class="fa fa-eye"></i>Detalle</a>
                                        </th>-->
                                    </tr>
                        
                                    <?php } ?>
                            
                                    <?php if($students->count() == 0) { ?>
                                            <tr style="cursor: pointer;"><td colspan="4"><span class="col-md-12 text-center text-warning">No hay registro que mostrar</span></td></tr>
                                    <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="center">
                        {!! $students->render() !!}                    
                    </div>
                </div>
            </div>
        </section>
</div>
@endsection