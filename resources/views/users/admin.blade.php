@extends('layouts.app')

@section('content')
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Usuarios</li>
        </ol>
        <hr>
        <section id="course-list">
        <div class="panel panel-info">
            <div class="panel-heading">Usuarios <span class="label label-primary" title="{{$users->total()}} usuarios en total">{{$users->total()}}</span></div>
                <div class="panel-body">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-right">
                                        <div class="btn-group" role="group" >
                                            <button  id="btn-new" type="button" class="btn btn-default btn-xs"><i class="fa fa-plus" aria-hidden="true"></i> Nueva</button>                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                
                                <form action="{{url('admin/users')}}" method="get" autocomplete="off" role="form" class="form-inline">
                                    <div class="col-md-5">
                                        <div class="input-group">
                                            <input name="search" value="{{$search}}" type="text" class="form-control" placeholder="Ingresar documento, correo o nombre">
                                            <span class="input-group-btn">                                    
                                                <button type="submit" class="btn pull-right"><i class="fa fa-search"></i></button>
                                            </span>                               
                                        </div>
                                    </div>                          
                                </form>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover course-list-table tablesorter table-sm">
                                    <thead>
                                    <tr>
                                        <th  class="starts">#</th>
                                        <th class="starts">Nombre</th>
                                        <th class="starts">Correo</th>
                                        <th class="starts">Activo</th>
                                        <th class="starts">Opciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($users as $user) {?>                                
                                            <tr>
                                                <th>{{$user->id}}</th>                                      
                                                <th><?= "{$user->name} {$user->surname} {$user->second_surname}" ?></th>
                                                <th>{{$user->email}}</th>
                                                <th>{{$user->deleted == 0 ? "SI" :"NO"}}</th>
                                                <th>
                                                    <a type="button" data-id="{{$user->id}}" class="btn btn-edit  btn-small" title="Actualizar Artículo"><i class="fa fa-eye"></i>Detalle</a>
                                                    @if($user->deleted == 0 and $user->id != 1)
                                                        <a  type="button"  data-id="{{$user->id}}" class="btn btn-delete btn-small btn-danger" title="Elimnar"><i class="fa fa-trash-o"></i>Eliminar</a>
                                                    @endif
                                                </th>
                                            </tr>
                                        <?php } ?>
                                        <?php if($users->count() == 0) { ?>
                                            <tr style="cursor: pointer;"><td colspan="5"><span class="col-md-12 text-center text-warning">No hay registro que mostrar</span></td></tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="center">
                                {!! $users->render() !!}                    
                            </div>
                        </div>
                    </div>
                </div>
            <div class="modal fade" id="modalUser" tabindex="-1" role="dialog" aria-labelledby="modalAreasLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-black">
                            <h5 class="modal-title" id="modalAreasLabel">Datos de usuario</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal-body">
                            <form autocomplete="off" id="form-user" method="post">
                                @csrf
                                <div class="form-group row">
                                    <label for="name" class="col-sm-3 text-danger col-form-label">Nombres:*</label>
                                    <div class="col-sm-9">
                                        <input type="text" id="name" required class="form-control-plaintext" name="name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-sm-3 text-danger col-form-label">Apellido 1:*</label>
                                    <div class="col-sm-9">
                                        <input type="text" id="surname" required class="form-control-plaintext" name="surname">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-sm-3 text-danger col-form-label">Apellido 2:*</label>
                                    <div class="col-sm-9">
                                        <input type="text" id="second_surname" required class="form-control-plaintext" name="second_surname"  >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-sm-3 text-danger col-form-label">Correo:*</label>
                                    <div class="col-sm-9">
                                        <input type="email" required id="email" class="form-control-plaintext" name="email" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="password" class="col-sm-3 col-form-label">Contraseña:*</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control-plaintext" name="password" >
                                    </div>
                                </div>
                                
                                <div class="text-right">
                                    <hr>
                                    <button type="submit" id="btn-save" class="btn btn-primary">Guardar</button>
                                </div>
                            </form>
                        </div>                    
                    </div>
                </div>
            </div>

        </section>
</div>
@endsection

@section('js')
<script>
    const UTIL_USER ={
        id:-1,
        get_user: (id)=>{
            
            $.get("{{url('admin/users/get-user')}}/"+id,{}, (resp)=>{
                UTIL_USER.id = id;
                $("#name").val(resp.user.name);
                $("#surname").val(resp.user.surname);
                $("#second_surname").val(resp.user.second_surname);
                $("#email").val(resp.user.email);

                $("#modalUser").modal("show");
            })
        }
    }
    $(window).load(function () {
        $("#btn-new").click(function () {
            UTIL_USER.id = -1;         
            $("#modalUser").modal("show");
            $("#email").prop('readonly', false);
            
        });
        $(".btn-delete").click(function (e) {
            let id = $(this).attr("data-id");
            swal({
                title: "¿Estás seguro que deseas eliminar el usuario #"+id +" ?",
                text: "Estos cambios no se puede deshacer!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        axios.post(`/admin/users/delete-user/${id}`).then(resp => {
                            location.href = location.href;
                        });
                    }
            });
           
        })
        $(".btn-edit").click(function (e) {
            let id = $(this).attr("data-id");
            UTIL_USER.get_user(id); 
            $("#btn-save").prop('disabled', false);
            $("#email").prop('readonly', true);
        });
       
       
       
        $("#form-user").validate({
            rules: {
                name : {
                    required: true,
                    minlength: 3,
                    maxlength:100
                },
                second_surname : {
                    required: true,
                    minlength: 3,
                    maxlength:100
                },
                surname : {
                    required: true,
                    minlength: 3,
                    maxlength:100
                },
                email : {
                    required: true,
                    email: true
                }                   
            },
            messages : {
                name: {
                    required: "Nombre es requerido"                   
                },
                surname: {
                    required: "Apellidos 1 es requerido"                   
                },
                second_surname: {
                    required: "Apellidos 2 es requerido"            
                },
                email: {
                    required: "Correo es requerido" ,
                    email:"No es un correo valido"                  
                }
            }
        });

        $('#form-user').submit(function(e) {
            e.preventDefault() ;
            if($("#form-user").valid()){
                $("#btn-save").prop('disabled', true);
                var formData =$('#form-user').serialize() ; 
                $.ajax({
                    url: "{{route('users.create-user')}}/"+UTIL_USER.id ,
                    type: "post",
                    dataType: "json",
                    data: formData,
                    success: function(resp) {
                    /// resp = JSON.parse(resp);
                    if(resp.success){
                        $('#form-user')[0].reset();
                        location.href = location.href;
                    }                  
                    },
                    error: function (request, status, error){
                        let errors = request.responseJSON.errors;
                        if(errors != null){
                            for (const key in errors) {
                                let error = errors[key];
                                for (const key2 in error) {
                                    toastr.error(error[key2], 'error');
                                }
                            }
                        }
                    },
                    complete:function(xhr, status){
                        $("#btn-save").prop('disabled', false);
                    }
                });  
            }          
        });
        
    });

   
</script>

@endsection