
@extends('layouts.app')

@section('content')
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Banco de preguntas</li>
        </ol>
        <hr>
        <section id="course-list">
            <div class="panel panel panel-info">
                <div class="panel-heading">Preguntas de test <span class="label label-primary" title="{{$questions->total()}} usuarios en total">{{$questions->total()}}</span></div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <div class="btn-group" role="group" >
                                    <a  type="button" href="{{ url('admin/questions/question-view/-1')}}" class="btn btn-default btn-xs"><i class="fa fa-plus"></i> Nueva</a>                           
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    
                        <form action="{{url('admin/questions')}}" method="get" autocomplete="off" role="form" class="form-inline">
                            <div class="col-md-5">
                                <div class="input-group">
                                    <input name="search" value="{{$search}}" type="text" class="form-control" placeholder="Ingresar una pregunta">
                                                                        
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <select class="form-control" name="area_id">
                                    <option value="">--Area--</option>
                                        <?php foreach ($areas as $area) { ?>
                                            <option <?= $area->id == $area_id ? "selected":""?>  value="<?=$area->id?>"><?=$area->name?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="input-group-btn">
                                    
                                        <button type="submit" class="btn pull-right"><i class="fa fa-search"></i></button>
                                    </span>
                                    
                                </div>
                            </div>
                        </form>
                    </div>
                
                    <div class="table-responsive">
                        <table class="table table-hover course-list-table tablesorter table-sm">
                            <thead>
                            <tr>
                                
                                <th class="starts">ID</th>
                                <th class="starts">Prengunta</th>
                                <th class="starts">Area</th>
                                <th class="starts">Tipo</th>
                                <th class="starts">Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($questions as $question) {?>
                                
                                    <tr>                                        
                                        <th><b>{{$question->id}}</b></th>
                                        <th>{{$question->questions}}</th>
                                        <th>{{$question->area}}</th>
                                        <th>{{$question->type == 1 ? "S. Multiple":"Ordenamiento"}}</th>
                                        <th>
                                            <a href="{{url('admin/questions/question-view/'.$question->id)}}" class="btn  btn-small" title="Detalles"><i class="fa fa-eye"></i></a>
                                            <a  type="button"  data-id="{{$question->id}}" class="btn btn-delete btn-small btn-danger" title="Elimnar"><i class="fa fa-trash-o"></i></a>
                                        </th>
                                    </tr>
                        
                                    <?php } ?>
                                    <?php if($questions->count() == 0) { ?>
                                        <tr style="cursor: pointer;"><td colspan="5"><span class="col-md-12 text-center text-warning">No hay registro que mostrar</span></td></tr>
                                    <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="center">
                        {!! $questions->render() !!}
                    </div>    
                
                </div>
            </div>
        </section>
    </div>

@endsection

@section('js')
<script>
    
    $(window).load(function () {
       
        $(".btn-delete").click(function (e) {
            let id = $(this).attr("data-id");
            swal({
                title: "¿Estás seguro que deseas eliminar la pregunta?",
                text: "Estos cambios no se puede deshacer!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        axios.post(`/admin/questions/delete-question/${id}/`).then(resp => {
                            location.reload();
                        });
                    }
            });
        });
    });
   
</script>

@endsection