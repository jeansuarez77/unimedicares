@extends('layouts.app')

@section('content')
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li class="active"><a href="/admin/questions">Banco de preguntas</a> </li>
            <li class="active">Crear</li>
        </ol>
        <hr>

        <create-edit-component  :question_id="{{$question_id}}"></create-edit-component>
       
</div>

@endsection
