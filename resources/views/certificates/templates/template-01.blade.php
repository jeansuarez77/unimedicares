<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Certificado</title>
        <style>
         
         body{           
            margin: 0px;
            height: 100%;
            width: 100%;
            background-repeat: no-repeat;
            background: url('https://marketplace-facilpos.s3.us-east-2.amazonaws.com/img/certificado-unimedicares.png') no-repeat center center fixed;
            background-size: 100% 100%;
        }
        @page {margin:0px;}
        .contenedor{
            text-align: center;
        }
   
        
    </style>
    </head>
    <body >   
        
        <div style="text-align: center;padding-top: 175px;    margin-left: 140px; ">
            <h1 style="text-align: center; font-size: 27px; font-family: Arial, Helvetica, sans-serif">{{$name}}</h1>
            <h2 style="text-align: center;  font-size: 18px; font-family:  Arial, Helvetica, sans-serif">{{$document_type}}. {{$document}} DE {{$document_from}}</h1>
            <p style="text-align: center;  margin: 0px; font-size: 14px !important;   Arial, Helvetica, sans-serif" >ASISTIÓ Y ADQUIRIÓ CONOCIMIENTOS Y HABILIDADES EN EL CURSO TALLER DE</p>
            <h1 style="text-align: center;  font-size: 27px; margin-top: 12px; argin-bottom: 10px;">{{$course_name}}</h1>
            <p style="text-align: center;  margin: 0px;font-family:  Arial, Helvetica, sans-serif;  font-size: 14px !important;" >{{$relize_in}}</p>
            <p style="text-align: center;  margin: 0px;font-family:  Arial, Helvetica, sans-serif;  font-size: 14px !important;">{{$vigencia}}</p>
            <p><b>{{$given_in}}</b></p>
        </div>       
    </body>
</html>