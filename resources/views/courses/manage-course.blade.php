@extends('layouts.app')

@section('content')

    <div class="container">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li >Cursos</li>
            <li class="active">Gestión</li>
        </ol>
        <hr >
        
        <gestion-component :area_id="'{{$course->area_id}}'" :name_course="'{{$course->name}}'" :course_id="{{$course_id}}"></gestion-component>
    </div>



@endsection


@section('js')

@endsection