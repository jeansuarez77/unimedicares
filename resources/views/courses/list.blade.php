
@extends('layouts.app')

@section('content')
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Cursos</li>
        </ol>
        <hr>
        <section id="course-list">
            <div class="panel panel panel-info">
                <div class="panel-heading">Cursos <span class="label label-primary" title="{{$courses->total()}} cursos en total">{{$courses->total()}}</span></div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <div class="btn-group" role="group" >
                                    <a href="{{url('admin/courses/areas')}}" class="btn btn-default btn-xs" ><i class="fa fa-plus-circle"></i>Areas</a>
                                    <a  type="button" href="{{ url('admin/courses/courses-view/-1')}}" class="btn btn-default btn-xs"><i class="fa fa-plus"></i> Nueva</a>                           
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">                    
                        <form action="{{url('admin/courses')}}" autocomplete="off" method="get" role="form" class="form-inline">
                            <div class="col-md-5">
                                <div class="input-group">
                                    <input name="search" value="{{$search}}" type="text" class="form-control" placeholder="Ingresar nombre area o código">
                                                                        
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <select class="form-control" name="area_id">
                                    <option value="">--Area--</option>
                                        <?php foreach ($areas as $area) { ?>
                                            <option <?= $area->id == $area_id ? "selected":""?>  value="<?=$area->id?>"><?=$area->name?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="input-group-btn">
                                       
                                        <button type="submit" class="btn pull-right"><i class="fa fa-search"></i></button>
                                    </span>
                                    
                                </div>
                            </div>
                        </form>
                    </div>
                
                    <div class="table-responsive">
                        <table class="table table-hover course-list-table tablesorter table-sm">
                            <thead>
                                <tr>                                
                                    <th class="starts">Nombre</th>
                                    <th class="starts">Código</th>
                                    <th class="starts">Area</th>
                                    <th class="starts">Precio</th>
                                    <th class="starts">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($courses as $course) {?>
                                
                                    <tr>
                                        
                                        <th><b>{{$course->name}}</b></th>
                                        <th>{{$course->code}}</th>
                                        <th>{{$course->area}}</th>
                                        <th>
                                            <?php if($course->is_free) {?>
                                            <span class='label label-primary'>Gratis</span>
                                            <?php } else { 
                                                echo "$". number_format($course->price, 2, ",", ".");
                                            } ?>
                                            </th>
                                        <th>
                                            <a href="{{url('admin/courses/courses-view/'.$course->id)}}" class="btn  btn-small" title="Detalles"><i class="fa fa-eye"></i></a>
                                            <a href="{{url('admin/courses/courses-manage/'.$course->id)}}" class="btn  btn-small" title="Gestionar"><i class="fa fa-cogs"></i></a>
                                            <a  type="button"  data-id="{{$course->id}}" class="btn btn-delete btn-small btn-danger" title="Elimnar"><i class="fa fa-trash-o"></i></a>
                                        </th>
                                    </tr>
                        
                                <?php } ?>

                                <?php if($courses->count() == 0){ ?>
                                    <tr style="cursor: pointer;"><td colspan="5"><span class="col-md-12 text-center text-warning">No hay registro que mostrar</span></td></tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="center">
                        {!! $courses->render() !!}
                    </div>
                </div>
            </div>
        </section>
    </div>

  
    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="modalAreasLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header"></div>
                
                <div class="modal-body">
                    <h4>¿Quieres borrar el curso?</h4>        
                    <div class="text-center">
                        <hr>
                        <button type="button" id="btn-d-yes" class="btn btn-danger btn-xs">Si</button>
                        <button type="button" id="btn-d-no"class="btn btn-primary btn-xs">No</button>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>
    const UTIL_COURSE ={
        id:-1
    }
    $(window).load(function () {
        $("#btn-d-no").click(function (e) {
            $("#modalDelete").modal("hide");
        });
        $(".btn-delete").click(function (e) {
            let id = $(this).attr("data-id");
            UTIL_COURSE.id = id;
            $("#btn-d-yes").prop('disabled', false);
            $("#modalDelete").modal("show");
        });
        $("#btn-d-yes").click(function (e) {
            $("#btn-d-yes").prop('disabled', true);
            $.ajax({
                url: "{{url('admin/courses/delete-course')}}/"+UTIL_COURSE.id,
                type: "delete",
                data: {_token:"{{ csrf_token() }}"},
                success: function(resp) {
                   if(resp.success){                        
                        location.href = location.href;
                   }
                }, 
                complete:function(xhr, status){
                    $("#btn-d-yes").prop('disabled', false);
                }
            });
       
      
        });
        
    });

   
</script>

@endsection