@extends('layouts.app')

@section('content')
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Cursos</li>
            <li class="active">Crear</li>
        </ol>
        <hr>
        <section id="area-list">
            <div class="panel panel panel-info">
                <div class="panel-heading">Datos del curso </div>
                <div class="panel-body">
                    <form id="from-course" class="text-black" enctype="multipart/form-data" action="{{url('admin/courses/save-course/'.$course_id)}}" method="post" autocomplete="off">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 text-danger col-form-label">Nombre:*</label>
                            <div class="col-sm-10">
                                <input  type="text" class="form-control-plaintext" value="{{ old('name', $couerse->name) }}" name="name" >
                                @error('name')
                                <label for="name" class="error">{{$message}}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="code" class="col-sm-2 text-danger col-form-label">Código:*</label>
                            <div class="col-sm-10">
                                <input required value="{{ old('code', $couerse->code) }}" type="text" class="form-control-plaintext" name="code">
                                @error('code')
                                    <label for="code" class="error">{{$message}}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="area_id" class="col-sm-2 text-danger col-form-label">Area:*</label>
                            <div class="col-sm-10">
                                <select required name="area_id" class="form-control">
                                    <?php foreach ($areas as $area ){ ?>
                                        <option <?=  old('area_id', $couerse->area_id)  == $area->id ? "selected" :"" ?>   value="{{$area->id}}">{{$area->name}}</option>
                                    <?php } ?>
                                </select>
                                @error('area_id')
                                    <label for="area_id" class="error">{{$message}}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="code" class="col-sm-2 text-danger col-form-label">Intensidad (Horas):*</label>
                            <div class="col-sm-10">
                                <input required value="{{ old('intensity', $couerse->intensity) }}" type="number" class="form-control-plaintext" name="intensity">
                                @error('intensity')
                                    <label for="code" class="error">{{$message}}</label>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="code" class="col-sm-2 text-danger col-form-label">Vigencia (días):*</label>
                            <div class="col-sm-10">
                                <input required value="{{ old('validity', $couerse->validity) }}" type="number" class="form-control-plaintext" name="validity">
                                @error('validity')
                                    <label for="validity" class="error">{{$message}}</label>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-sm-2 col-form-label">Es gratis ?:</label>
                            <div class="col-sm-10">                                
                                <input type="checkbox" name="is_free"  value="1" <?=  old('is_free', $couerse->is_free)  == 1 ? "checked" :"" ?> class="form-check-input">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-2 col-form-label">Precio:</label>
                            <div class="col-sm-10">                                
                                <input type="number"   name="price"  value="{{old('price', $couerse->price)}}" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="logo" class="col-sm-2 text-danger col-form-label">Logo:*</label>
                            <div class="col-sm-10">
                                <input  accept="image/png/jpeg/jpg" type="file" <?= $course_id < 1 ? "required": ""?> class="form-control"  name="logo" placeholder="logo">
                                @error('logo')
                                    <label for="logo" class="error">{{$message}}</label>
                                @enderror
                            </div>
                        </div>
                        
                        <div id="div-description" style="display: none;" class="form-group row">
                            <label for="description" class="col-sm-2 text-danger col-form-label">Descripción:*</label>
                            <div class="col-sm-10">
                                <textarea required name="description" id="description">{{old("description",$couerse->description)}}</textarea >
                            </div>

                            @error('description')
                                    <label for="description" class="error">{{$message}}</label>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="file1" class="col-sm-2 col-form-label text-danger ">Video de presentación:*</label>
                            <div class="col-sm-10">
                                <input  accept="video/mp4,video/wmv,video/avi,video/*"
                                    id="file1" <?= $course_id < 1 ? "required": ""?> type="file" class="form-control"  name="file1" >
                                @error('file1')
                                    <label for="file1" class="error">{{$message}}</label>
                                @enderror
                            </div>

                            
                        </div>

                        <?php if(!empty($couerse->img_vide) || !empty($couerse->logo)) {?>
                            <div class="form-group row">
                            
                                <label for="logo" class="col-sm-2 col-form-label"></label>
                                
                                <?php if(!empty($couerse->img_vide) ) {?>
                                    <div class="col-sm-5">
                                        <video src="{{$couerse->img_vide}}" style="max-width: 100%; max-height: 300px;" controls ></video>
                                    </div>
                                <?php } ?>
                                <?php if(!empty($couerse->logo) ) {?>
                                    <div class="col-sm-5">                                
                                        <img class="img-thumbnail" src="{{$couerse->logo}}" style="max-height: 300px;" id="img-logo">
                                    
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>

                    
                        <div class="text-right">
                        
                            <button type="submit" style="display: none"id="btn-save" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
</div>
@endsection
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">

@section('js')

<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>


<script>
    $(document).ready(function() {
       $("#btn-save").show();
        $('#description').summernote({
            height: 200,   //set editable area's height
            codemirror: { // codemirror options
                theme: 'monokai'
            }
        });
        $("#from-course").validate({
            rules: {
                name : {
                    required: true,
                    minlength: 3,
                    maxlength:150
                },
                description : {               
                    required: true,
                },
                code : {
                    required: true,
                    minlength: 3,
                    maxlength:100
                },
                logo : {
                     required: <?= json_encode($course_id < 1 ? true: false)?>,
                     
                },
                file1 : { required: <?= json_encode($course_id < 1 ? true: false) ?>},
            },
            messages : {
                name: {
                    required: "Nombre es requerido",
                    minlength: "Mínimo 3 caracteres",
                    minlength: "Máximo 150 caracteres"
                },
                description: {
                    required: "la descripción es requerido",
                },
                code: {
                    required: "El código es requerido",
                },
                logo: {
                    required: "El logo es requerido",
                },
                file1: {
                    required: "el video  es requerido",
                }
            }
        });

       
        $("#div-description").show();
    });

   
  </script>
@endsection