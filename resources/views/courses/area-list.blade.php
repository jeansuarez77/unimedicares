@extends('layouts.app')

@section('content')
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Cursos</li>
        </ol>
        <hr>
        <section id="area-list">
            <div class="panel panel panel-info">
                <div class="panel-heading">Areas <span class="label label-primary" title="{{$areas->total()}} areas en total">{{$areas->total()}}</div>                    

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <div class="btn-group" role="group" >
                                    <button  id="btn-new" type="button" class="btn btn-default btn-xs"><i class="fa fa-plus" aria-hidden="true"></i> Nueva</button>                           
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">                    
                        <form action="{{url('admin/courses/areas')}}" method="get" autocomplete="off" role="form" class="form-inline">
                            <div class="col-md-5">
                                <div class="input-group">
                                    <input name="search" value="{{$search}}" type="text" class="form-control" placeholder="Ingresar una pregunta">
                                    <span class="input-group-btn">                                    
                                        <button type="submit" class="btn pull-right"><i class="fa fa-search"></i></button>
                                    </span>                                
                                </div>
                            </div>                         
                        </form>
                    </div>
                    
                    <div class="table-responsive">
                        <table class="table table-hover course-list-table tablesorter table-sm" >
                            <thead>
                            <tr>
                                <th class="starts">Nombre</th>
                                <th class="starts">Logo</th>
                                <th class="starts">Descripción</th>
                                <th class="starts">Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($areas as $area) {?>                                
                                    <tr>
                                        <th class="course-title"><b>{{$area->name}}</b></th> 
                                        <th class="course-title"> <img  style="width: 60px;" class="img-circle" src="{{ asset($area->img_url )}}" ></th>                               
                                        <th>{{$area->description}}</th>                                
                                        <th>
                                            <a  type="button"  data-id="{{$area->id}}" class="btn btn-edit btn-small" title="Actualizar"><i class="fa fa-eye"></i>Detalle</a>
                                            <a  type="button"  data-id="{{$area->id}}" class="btn btn-delete btn-small btn-danger" title="Elimnar"><i class="fa fa-trash-o"></i>Eliminar</a>
                                        </th>
                                    </tr>                        
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="center">
                        {!! $areas->render() !!}
                    </div>
                </div>
            </div>
        </section>
    </div>

<div class="modal fade" id="modalAreas" tabindex="-1" role="dialog" aria-labelledby="modalAreasLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-black">
        <h5 class="modal-title" id="modalAreasLabel">Datos del area</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
      </div>
      <div class="modal-body">
        <form id="from-areas" method="post">
            @csrf
            <div class="form-group row">
                <label for="name" class="col-sm-2 text-danger col-form-label">Nombre:*</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control-plaintext" name="name" id="name" >
                </div>
            </div>
            <div class="form-group row">
                <label for="description" class="col-sm-2 text-danger col-form-label">Descripción</label>
                <div class="col-sm-10">
                    <textarea name="description" id="description" cols="30" rows="2"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="logo" class="col-sm-2 col-form-label">Logo:</label>
                <div class="col-sm-10">
                    <input type="file" class="form-control" id="logo" name="logo" placeholder="logo">
                </div>
            </div>
            <div class="form-group row">
                <label for="logo" class="col-sm-2 col-form-label"></label>
                <div class="col-sm-10">
                    
                    <img class="img-thumbnail" src="" style="width:100%" id="img-logo">
                </div>
            </div>
            <div class="text-right">
                <hr>
                <button type="submit" id="btn-save" class="btn btn-primary">Guardar</button>
            </div>
        </form>
      </div>
      
    </div>
  </div>
</div>
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="modalAreasLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header"></div>        
        <div class="modal-body">
            <h4>¿Quieres borrar el área?</h4>        
            <div class="text-center">
                <hr>
                <button type="button" id="btn-d-yes" class="btn btn-danger btn-xs">Si</button>
                <button type="button" id="btn-d-no"class="btn btn-primary btn-xs">No</button>
            </div>                    
        </div>      
    </div>
  </div>
</div>



@endsection

@section('js')
<script>
    const UTIL_AREA ={
        id:-1,
        get_area: (id)=>{
            
            $.get("{{url('admin/courses/get-area')}}/"+id,{}, (resp)=>{
                UTIL_AREA.id = id;
                //resp = JSON.parse(resp);
                $("#name").val(resp.area.name);
                $("#description").val(resp.area.description);
                $("#modalAreas").modal("show");
                $("#img-logo").show();
                $("#img-logo").attr("src",resp.area.img_url);

                if(resp.area.img_url = null || resp.area.img_url == "")
                    $("#img-logo").hide();
            })
        }
    }
    $(window).load(function () {
        $("#btn-new").click(function () {
            UTIL_AREA.id = -1;
            $("#img-logo").hide();
            $("#modalAreas").modal("show");
            
        });
        $(".btn-edit").click(function (e) {
            let id = $(this).attr("data-id");
            UTIL_AREA.get_area(id); 
            $("#btn-save").prop('disabled', false);
        });
        $("#btn-d-yes").click(function (e) {
            $("#btn-d-yes").prop('disabled', true);
            $.ajax({
                url: "{{url('admin/courses/delete-area')}}/"+UTIL_AREA.id,
                type: "delete",
                data: {_token:"{{ csrf_token() }}"},
                success: function(resp) {
                   if(resp.success){     
                       toastr.success('Area guardada', 'exito');                   
                        location.href = location.href;
                   }else{
                    toastr.error('Error al guardar', 'error');
                   }
                }, 
                complete:function(xhr, status){
                    $("#btn-d-yes").prop('disabled', false);
                }
            });
            
        });
        $("#btn-d-no").click(function (e) {
            $("#modalDelete").modal("hide");
        });
        $(".btn-delete").click(function (e) {
            let id = $(this).attr("data-id");
            UTIL_AREA.id = id;
            $("#btn-d-yes").prop('disabled', false);
            $("#modalDelete").modal("show");
        })
        $("#from-areas").validate({
            rules: {
                name : {
                    required: true,
                    minlength: 3,
                    maxlength:100
                },
                description : {               
                    maxlength:150
                }
            },
            messages : {
                name: {
                    required: "Nombre es requerido",
                    minlength: "Mínimo 3 caracteres",
                    minlength: "Máximo 100 caracteres"
                },
                description: {
                    minlength: "Máximo 150 caracteres"
                }
            }
        });

        $('#from-areas').submit(function(e) {
            e.preventDefault() ;
            $("#btn-save").prop('disabled', true);
            var formData = new FormData(document.getElementById("from-areas")); 
            $.ajax({
                url: "{{route('courses.create-area')}}/"+UTIL_AREA.id ,
                type: "post",
                dataType: "html",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(resp) {
                    resp = JSON.parse(resp);
                   if(resp.success){
                        $('#from-areas')[0].reset();
                        location.href = location.href;
                   };
                  
                },
                complete:function(xhr, status){
                    $("#btn-save").prop('disabled', false);
                }

            });
            
        });

        
        
    });

   
</script>

@endsection

