@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="card">

                <div class="card-body">
                    <section id="account-sign-in" class="col-md-6 col-md-offset-3" style="margin-top:20px">
                        <header><h2>Formulario de registro </h2></header>
                        <form autocomplete="off" method="POST" action="{{ route('register') }}">
                            @csrf


                            <div class="form-group">
                                <label for="document_type_id" class="text-danger">Tipo de documento*</label>

                                    <select name="document_type_id"  class="form-control @error('document_type_id') is-invalid @enderror" required  autofocus>
                                        <option value="" disabled selected>Sleccione un tipo de documento</option>
                                        @foreach ($types_documents as $item)
                                            <option value="{{$item->id}}">{{$item->code}} {{$item->name}}</option>
                                        @endforeach
                                        
                                    </select>
                                    @error('document_type_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>

                            <div class="form-group">
                                <label for="document" class="text-danger">Numero de documento*</label>

                                    <input id="document" type="text" class="form-control @error('name') is-invalid @enderror" name="document" value="{{ old('document') }}" required autocomplete="document" autofocus>

                                    @error('document')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>


                            <div class="form-group">
                                <label for="name" class="text-danger">Nombres completo*</label>

                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>

                            <div class="form-group">
                                <label for="surname" class="text-danger">Primer apellido*</label>

                                    <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ old('surname') }}" required >

                                    @error('surname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>

                            <div class="form-group">
                                <label for="second_surname" class="text-danger">Segundo apellido*</label>

                                    <input id="second_surname" type="text" class="form-control @error('second_surname') is-invalid @enderror" name="second_surname" value="{{ old('second_surname') }}" required >

                                    @error('second_surname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>
                            <div class="form-group">
                                <label  class="text-danger"> Ciudad</label>
                                <select id="city_id" name="city_id" class="form-control text-black">
                                    
                                   <option value="" disabled selected> Seleccione</option>
                                   @foreach ($cities as $item)
                                    <option value="{{$item->id}}">{{$item->name}} - {{$item->name_d}} - {{$item->country}}</option>
                                   @endforeach
                                </select>

                                @error('city_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                
                            </div>

                            <div class="form-group">
                                <label for="phone" >Celular</label>

                                    <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" autocomplete="off" >

                                    @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>
                            <div class="form-group">
                                <label for="email" class="text-danger" >Dirección de correo electronico*</label>

                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>

                            <div class="form-group">
                                <label for="password" class="text-danger" >Contraseña*</label>

                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="text-danger" >Confirmar contraseña*</label>

                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>

                            <div class="form-group mb-0">
                                <button type="submit" class="btn btn-primary">
                                    Registrarse
                                </button>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
    </div>
</div>
@endsection

@section('js')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
<script>
    $(function () {
        $("#city_id").select2({
            placeholder: "Seleccione ciudad de residencia",
        });
    });
</script>
    
@endsection
