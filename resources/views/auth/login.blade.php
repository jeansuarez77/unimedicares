@extends('layouts.app')

@section('content')
<div class="container" >
    <div class="row justify-content-center">
        <div class="card">
            <div class="card-body">
            
                <section id="account-sign-in" class="col-md-6 col-md-offset-3" style="margin-top:20px">
                    
                    <header><h2>Iniciar sesión </h2></header>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                       
                        <div class="form-group">
                            <label for="email">Correo Electronico</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"  placeholder="Ingrese su email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password">Contraseña</label>

                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Ingrese su contraseña" required autocomplete="current-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>

                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        Recuérdame
                                    </label>
                                </div>
                            </div>
                            @if (Route::has('password.request'))

                                <div class="col-md-6">
                                    <a class="btn-link text-white" href="{{ route('password.request') }}">
                                        ¿Olvidaste tu contraseña?
                                    </a>
                                </div>

                            @endif
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    Iniciar
                                </button>

                                

                                <a class="btn btn-link bg-success" href="{{ route('register') }}">
                                        Registrarme
                                </a>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</div>
@endsection
