@extends('layouts.app')

@section('content')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}">Inicio</a></li>
        <li class="active">Capacítate</li>
    </ol>
</div>
<div class="container" >
    <div class="row justify-content-center">
        <section id="main-content">
            
            <div class="section-content">
                
                <h2>Cursos</h2>

                <div class="row">
                    @foreach($areas as $area )
                        <div class="col-md-3" >
                            <a href="#instructors" onclick="get_courses_area('{{$area->id}}')" class="universal-button framed" style="padding: 10px;">
                                <h3>{{$area->name}}</h3>
                                <figure class="date"><i class="fa fa-arrow-right"></i></figure>
                            </a><!-- /.universal-button -->
                        </div><!-- /.col-md-6 -->
                    @endforeach

                    
                </div><!-- /.row -->


                <section class="author-carousel owl-theme" id="instructors">
                    <div class="owl-wrapper-outer">
                        <div class="owl-wrapper">
                            <div class="owl-item">
                                <div class="author">
                                    <div class="row" id="panel_courses_area">
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section><!-- /.author -->
                    <!-- /.author -->
                
            </div><!-- /.section-content -->
        </section><!-- /.main-content -->
    </div>
</div>

@endsection

@section('js')
<script>
    jQuery(document).ready(function() {
        
    });

    function get_courses_area(area){
        
        $("#panel_courses_area").html('<p class="col-md-12"> Por favor espere.... </p>');
        url_base = "{{url('')}}";

        $.get("<?= url("programas/courses_areas"); ?>/"+area,{},(resp)=>{
            //resp = JSON.parse(resp);
            var content = '';

            resp.courses.forEach(el => {
                content +=  `
                    <div class="col-md-2 text-center" style="height: 150px;margin-top: 10px;">
                            <a href="${url_base}/programas/curso/${el.url}" >
                                <img src="${el.logo ? url_base+el.logo :'assets/img/student-testimonial.jpg'}" class="logo-img" style="height: 100px;-webkit-border-radius: 50%;" alt="">
                                <h6 class="text-white"><b>${el.name}</b></h6>
                            </a>
                    </div>
                `; 
            });       

            $("#panel_courses_area").html(content); 
        });
    }
</script>

@endsection