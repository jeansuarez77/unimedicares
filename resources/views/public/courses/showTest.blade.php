<style>
  ol.questions_order {
  cursor: move !important;
}


.dragged {
  position: absolute;
  opacity: 0.5;
  z-index: 2000;
}

ol.questions_order li {
  border: 1px solid #c5c5c5 !important;
  background-color: #efeeee;

  margin: 0 3px 3px 3px; 
  font-size: 1.1em; 
}
ol.questions_order li.placeholder:before {
  position: absolute;
  /** Define arrowhead **/
}
</style>
<div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        <h4 class="modal-title" id="questions_orderModalLabel"> <b>{{$info_activity->name}}</b></h4>
      </div>

      <form class="text-black mt-1" id="form_test" action="{{route('course.save_test',['course_id' => $course_id , 'activity_id'=>$info_activity->id])}}" method="post" autocomplete="off">
        @csrf
        <div class="modal-body">
          <div class="row panel">
            <div class="col-md-12">
                <label id="porcent_q"></label>

            </div>
          </div>
            <div class="row panel" style="max-height: 600px; overflow-y: scroll">
                    <div class="col-md-12">
                            @foreach ($questions as $key => $item)
                                
                                <div class="col-md-12 mt-1">
                                    <label for="questions_orderInputPassword1" class="col-md-12">{{($key+1).'. '.$item->questions}}</label>

                                    <article class="col-md-12 ml-1">
                                      @if ($item->type == 2)

                                          <ol class='questions_order col-md-10' id="id_option_{{$item->id}}" data-id="{{$item->id}}">
                                            @foreach ($item->options as $option)
                                              <li id="{{$option->id}}"><span class="fa fa-arrows" style="color: #424242"> </span> {{$option->enunciate}}</li>
                                            @endforeach
                                          </ol>


                                      @else
                                      
                                        @foreach ($item->options as $option)
                                              
                                        <div class="radio col-md-12" id="id_option_{{$option->id}}">
                                            <label>
                                                <input type="radio"  required  name="questions[{{$item->id}}]" value="{{$option->id}}"> {{$option->enunciate}}
                                            </label>
                                        </div>

                                        @endforeach


                                      @endif
                                       
                                        

                                    </article>
                                </div>
                            
                            @endforeach
                    </div>
                    
                    
            </div>
        </div>
        <div class="modal-footer" id="bg-submit">
            <button type="submit" class="btn btn-secondary">Finalizar</button>
            <button type="button" class="btn btn-primary"  data-dismiss="modal">Cancelar</button>
        </div>
      </form>
     

    </div>
  </div>
  

  <script>
 jQuery(document).ready(function() {
        
        $("ol.questions_order").sortable();
              
      
  });
      
          
$('#form_test').submit(function (ev) {
        ev.preventDefault();

        let url = $('#form_test').attr('action');
        data_order = document.querySelectorAll('.questions_order');
        
        let questions_order = [];

        data_order.forEach(function(item) {
          dato = document.querySelectorAll('#'+item.id +' li');
            array_option = [];

            dato.forEach(function(value) {
              array_option.push(value.id);

            });
            questions_order.push({id:item.getAttribute("data-id"), option:array_option});

        });

        let data = $('#form_test').serialize()  + '&questions_order=' + JSON.stringify(questions_order);


        $.post(url , data,
         function (res) { 

          data =  res.response;
          data.forEach(function(item) {


            if (!item.is_check) {
              $("#id_option_"+item.id).addClass( "bg-danger" );
            }else{
              $("#id_option_"+item.id).addClass( "bg-success");
            }  

          });

          $("#porcent_q").html("<h3>Tu calificación es: <b>"+res.porcent_q+"% de 100% </b> </h3>");
          $("#bg-submit").html(''); 

        });
    });
  </script>