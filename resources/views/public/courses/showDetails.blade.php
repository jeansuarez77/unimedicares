@extends('layouts.app')

@section('content')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}">Inicio</a></li>
        <li><a href="{{route('courses.index')}}">Capacítate</a> </li>
        <li class="active">{{$course->name}}</li>
    </ol>
    <a href="{{route('courses.course',['url'=>$course->url])}}" class="btn btn-color-primary btn-small float-right">INTRODUCCIÓN DEL CURSO</a>
    @if (!$my_course_info)
        <a onclick="start_course()" class="btn btn-color-primary btn-small float-right">INICIAR CURSO</a>
    @endif
</div>
<div class="container mt-1">
    <div class="row">
        <!-- Course Image -->

        @if (!empty($info_activity))
            <div class="col-md-12">

                @if ($info_activity->type == 2)
                    <div style="height: 90vh" class="col-sm-12">
                        <iframe src='{{asset($info_activity->file_url)}}' width="100%" height="100%" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
                    </div>
                @elseif($info_activity->type == 4)
                    <audio controls>
                        <source src="{{asset($info_activity->file_url)}}">
                    </audio>
                @else
                    <div  style="float: left;" class="col-md-6 col-xs-12">
                        <iframe src="{{asset($info_activity->file_url)}}" sandbox width="100%" height="400" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
                    </div>

                @endif

                <div class="mt-1 {{$info_activity->type == 2? 'col-md-12':''}}">
                        <!-- /.course-start -->
                        <div class="">
                            <h3 class="count-down-wrapper"> <b>{{$info_activity->name}} </b> </h3><!-- /.count-down-wrapper -->
                            <div class="panel-body text-justify">
                                {!!nl2br(e($info_activity->description))!!}
                            </div>
                        </div>
                </div><!-- /#course-header -->

            </div>
        @else
            <div class="col-md-12" >
                <div  style="float: left;" class="col-md-6 col-xs-12">
                    <iframe src="{{asset($course->img_vide)}}" width="100%" sandbox height="400" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
                </div>
                
                <section id="course-header">
                    
                    <div class="course-count-down disable-join" id="course-count-down">
                        <!-- /.course-start -->

                        <div class="count-down-wrapper">{{$course->name}}</div><!-- /.count-down-wrapper -->

                        @if ($my_course_info)
                            
                            <div class="course-progress">
                                <div class="progress">
                                    <div class="progress-bar text-center" role="progressbar" aria-valuenow="{{$porcen_course}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$porcen_course}}%;">
                                        {{$porcen_course}}%
                                    </div>
                                </div><!-- /.course-progress -->
                            </div><!-- /.course-progress -->

                        @endif
                        
                        <hr>

                        <p class="text-justify">
                            {!!$course->description!!}
                        </p>

                        
                    </div><!-- /.course-count-down -->
                    <hr>
                    <figure>
                        <span class="course-summary" id="course-length"><i class="fa fa-calendar-o"></i> Duración {{$course->intensity }} Horas</span>
                    </figure>
                </section><!-- /#course-header -->

            </div>
        @endif
        


        
        
        <!-- end Course Image -->
        <!--MAIN Content-->
        <div class="col-md-12">
            <div id="page-main">
                <hr>
                <section id="shortcodes" class="mt-1">
                    <article>
                        <div class="panel-group" id="accordion">

                        @foreach($levels as $key => $level)
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#question-{{$key+1}}">
                                            <span class="question">Nivel {{$key+1}}</span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="question-{{$key+1}}" class="panel-collapse {{(!empty($info_activity->level_id) && $level->id == $info_activity->level_id) ? 'in' : 'collapse'}}">
                                    
                                    @foreach($level->lessons as $key2 => $lesson)
                                        <div class=" panel-body mt-1">
                                            
                                            <div class="col-md-2 text-center">
                                                <p style="margin-top: 50px;" class="text-black"> <b> Leccion {{$key2+1}} </b>  </p>
                                            </div>

                                            @foreach ($lesson->activities as $activity)
                                                <div class="text-center mr-1 activity_icon" style="float: left">
                                                    @if ($activity->type == 3)

                                                        <a href="javascript:show_tets('{{$course->id}}','{{$activity->id}}')">
                                                                <img class="logo-img {{$activity->id == $activity_id? 'selected_' : ''}}" title="Test de evaluación"   style="{{$activity->seen==1 ?'background-color: teal':''}}" src="{{asset('assets/img/logos/test.png')}}"  alt="">
                                                        </a>
                                                    @else
                                                        
                                                        <a href="{{ route('courses.course',['url'=>$course->url, 'activity_id' => $activity->id]) }}">
                                                            <?php if($activity->type == 4){ ?>
                                                                <img class="logo-img {{$activity->id == $activity_id? 'selected_' : ''}}" title="Contenido multimedia" style="{{$activity->seen==1 ?'background-color: teal':''}}" src="{{asset('assets/img/logos/audio.png')}}" alt="">
                                                            
                                                            <?php } else if($activity->type == 2){ ?>
                                                                <img class="logo-img {{$activity->id == $activity_id? 'selected_' : ''}}" title="Infografias"  style="{{$activity->seen==1 ?'background-color: teal':''}}" src="{{asset('assets/img/logos/infografia.png')}}"  alt="">

                                                            <?php }else if($activity->type == 1){ ?>

                                                                <img class="logo-img {{$activity->id == $activity_id? 'selected_' : ''}}" title="Contenido multimedia" style="{{$activity->seen==1 ?'background-color: teal':''}}" src="{{asset('assets/img/logos/video.png')}}" alt="">

                                                            <?php } ?>


                                                        </a>

                                                    @endif
                                                </div>
                                            @endforeach
                                            

                                        </div>
                                    @endforeach
                                </div>
                            </div>

                        @endforeach
                            
                        </div>
                    </article>
                </section><!-- /.shortcodes -->

                <section id="shortcodes" class="mt-1">
                    <article>
                        <div class="panel-group" id="accordion">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#plan">
                                            <span class="question"><b> Plan de capacitación </b> </span>
                                        </a>
                                    </h4>
                                </div>

                                
                       
                            
                                <div id="plan" class="panel-collapse collapse">
                                    <div class=" panel-body mt-1">
                                  
                                        <table class="table table-hover course-list-table tablesorter text-black">
                                            <caption class="text-black text-center">Plan de capacitación</caption>
                                            
                                            <tbody>

                                                @foreach($levels as $key => $level)
                                                <tr>
                                                    <td class="center" align="center" style="vertical-align: middle;">Nivel {{$key+1}} <br></td>
                                                    <td><table class="table text-black" style="background-color: rgba(255, 255, 255, 0); margin:0px"><tbody>

                                                    @foreach($level->lessons as $key2 => $lesson)
                                                        <tr>
                                                            <td class="center" style="vertical-align: middle;">Leccion {{$key2+1}} <br> 
                                                            <td style="vertical-align: middle;">
                                                                <ul>
                                                                    @foreach ($lesson->activities as $activity)
                                                                    <li class="div_mapa">
                                                                        {{ $activity->name }}
                                                                    </li>
                                                                    @endforeach
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody></table>
                                                    </td>
                                                </tr>
                                                @endforeach
                                          
                                            
                                            </tbody>

                                        </table>

                                    </div>
                                </div>
                            </div>

                            
                        </div>
                    </article>
                </section><!-- /.shortcodes -->
            </div><!-- /#page-main -->
        </div><!-- /.col-md-8 -->

        <div class="modal fade" id="modal_test"  data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        
        </div>

    </div><!-- /.row -->
</div>

@endsection

@section('js')
<script>
   

    function start_course() {
        var url = "<?= route('course.start',['id'=>$course->id]) ?>";

        $.get( url, function( data ) {

            if(!data.error){
                toastr.success(data.msg, 'Exito!');
                location.reload();
            }
            toastr.error(data.msg, 'Error!');
            
        });
    }

    function show_tets(id_curso,id_activity) {
        

        $.get( "{{url('programas/curso/show_tets')}}",{ course_id:id_curso, id_activity:id_activity}, function( data ) {
            
            if(data.error){
                toastr.error(data.msg, 'Error!');
               return false; 
            }
            $('#modal_test').modal('toggle')
            $('#modal_test').html(data.html);
        });
    }

    


    
</script>

@endsection