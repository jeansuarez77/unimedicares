@extends('layouts.app')

@section('content')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}">Inicio</a></li>
        <li><a href="{{route('courses.index')}}">Capacítate</a> </li>
        <li class="active">{{$activity->name}}</li>
    </ol>
</div>
<div class="container mt-1">
    <div class="row">
        
        <!-- end Course Image -->
        <!--MAIN Content-->
        <div class="col-md-10">
            <div class="row mt-1">
                <div style="height: 100vh" class="col-sm-12"  >
                  <iframe height="100%" width=100% src='{{asset($activity->file_url)}}' webkitallowfullscreen allowfullscreen></iframe>
                </div>
                <div class="col-sm-12  mt-1">
                  <div class="panel panel-info">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                      Building a PDF Viewer with Bootstrap is relatively straightforward,
                      but once you want to start annotating, signing, or filling forms,
                      you would have to implement these things yourself.
                    </div>
                  </div>
                </div>
              </div>
        </div><!-- /.col-md-8 -->

    </div><!-- /.row -->
</div>

@endsection

@section('js')
<script>
    jQuery(document).ready(function() {
        
    });

    
</script>

@endsection