@extends('layouts.app')

@section('content')
   <div id="slider">
      <div class="container">
         <div class="container">
            <div class="slider-wrapper">
               <div class="row">
                 
                  <!-- /.col-md-9 -->
                  <div class="col-md-9 col-sm-12">
                     <div class="image-carousel">
                        <iframe width="100%" height="400" src="https://www.youtube.com/embed/bo9Z_pgByQY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                     </div>
                  </div><!-- /.col-md-9 -->
                 
                  <div class="col-md-3 col-sm-12" style="min-height: 320px;">
                     <aside class="news-small" id="news-slider">
                        
                        <div class="section-content">
                           <article>
                              <header><i class="fa fa-graduation-cap" style="font-size:36px"></i>
                                 <a href="javascript:;" class="btn" style="background-color: rgb(170, 11, 11); border-color:brown ; border-radius: 5px">Educacion y cultura</a>
                              </header>
                           </article>
                           <!-- /article -->
                           <article>
                              <header><i class="fa fa-users" style="font-size:36px"></i>
                                 <a href="{{route('courses.index')}}" class="btn  btn-color-primary" style="background-color: rgb(10, 18, 128);border-color: rgb(10, 18, 128); border-radius: 5px">Capacitate</a>
                              </header>
                           </article>
                           <!-- /article -->
                           <article>
                              <header><i class="fa fa-heart" style="font-size:36px"></i>
                                 <a href="javascript:;" class="btn btn-success" style="background-color: rgb(10, 121, 16); border-color:darkgreen ; border-radius: 5px">Salud</a>
                              </header>
                           </article>
                           <!-- /article -->
                        </div>
                        <!-- /.section-content -->
                     </aside>
                     <!-- /.news-small -->
                  </div>
                  <!-- /.col-md-3 -->
               </div>
               <!-- /.row -->
            </div>
            <!-- /.slider-wrapper -->
         </div>
         <!-- /.slider-wrapper -->
      </div>
      <!-- /.container -->
   </div>
   <!-- end Slider -->
   <!-- Content -->
   <div class="block">
      <div class="container">
         <div class="row">
           
            <!-- /.col-md-3 -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container -->
   </div>

@endsection