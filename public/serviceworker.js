var staticCacheName = "pwa-v" + new Date().getTime();
var filesToCache = [
    '/offline',
    '/assets/js/jquery-2.1.0.min.js',
    '/assets/bootstrap/js/bootstrap.min.js',
    '/assets/bootstrap/css/bootstrap.css',
    '/assets/css/style.css',
    'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
    'http://fonts.googleapis.com/css?family=Montserrat:400,700',
    '/assets/img/enchufe.png',
    '/css/app.css',
    '/js/app.js',
    '/images/icons/icon-72x72.png',
    '/images/icons/icon-96x96.png',
    '/images/icons/icon-128x128.png',
    '/images/icons/icon-144x144.png',
    '/images/icons/icon-152x152.png',
    '/images/icons/icon-192x192.png',
    '/images/icons/icon-384x384.png',
    '/images/icons/icon-512x512.png',
];

// Cache on install
self.addEventListener("install", event => {
    this.skipWaiting();
    event.waitUntil(
        caches.open(staticCacheName)
            .then(cache => {
                return cache.addAll(filesToCache);
            })
    )
});

// Clear cache on activate
self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames
                    .filter(cacheName => (cacheName.startsWith("pwa-")))
                    .filter(cacheName => (cacheName !== staticCacheName))
                    .map(cacheName => caches.delete(cacheName))
            );
        })
    );
});

// Serve from Cache
self.addEventListener("fetch", event => {
    event.respondWith(
        caches.match(event.request)
            .then(response => {
               
                if(!navigator.onLine ){
                    let url = event.request.url;
               
                    let includes = [
                        ".png",".css",".js",".jpg",":400,700",".json",".woff2"
                    ];
                    let is_include = false;
                    for (const key in includes) {                       
                        if(url.includes(includes[key])){
                            is_include = true;
                            break;
                        }
                    }

                    if(!is_include)
                        return caches.match('offline');
                }

                return response || fetch(event.request);
                
            })
            .catch(() => {
                return caches.match('offline');
            })
    )
});