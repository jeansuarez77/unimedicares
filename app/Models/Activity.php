<?php

namespace App\Models;
use  DB;


use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $connection = 'mysql';
    protected $table  = 'activities';
    protected $hidden = [
        'created_at', 'updated_at'
    ];  
   // public $timestamps = false;

    protected $fillable = ["lesson_id","name","type","file_url","trial_numbers","order","description","deleted","number_questions"];

    // informacion de de actividad

    public static function getInfoActivity($activity_id)
    {
        
        $info = DB::table("activities as a")->select("a.*","l.level_id")
            ->join("lessons AS l","l.id" ,"=", "a.lesson_id")
            ->where('a.id',$activity_id)
            ->where('a.deleted',0)->first();
                    
        return $info;

    }

}

?>
