<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use DB;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'surname',
        'second_surname',
        'document',
        'document_type_id',
        'country_code',
        'department_id',
        'city_id',
        'phone',
        'date_birth',
        "document_city_id",
        "rol"
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function full_name($user){
        return "{$user->name} {$user->surname} {$user->second_surname}";

    }

    public static function is_data_valid($user){
        return !empty($user->document) and 
        !empty($user->document_type_id) and 
        !empty($user->name) and
        !empty($user->surname) and 
        !empty($user->document_city_id);
    }

    public static function getInfoUser($id){
        $query = DB::table("users as u")->select("u.*", "c.name as city", "d.name as name_d", "co.name as country", "doc.code as code_doc")
            ->join("types_documents AS doc","doc.id" ,"=", "u.document_type_id","left")
            ->join("cities AS c","c.id" ,"=", "u.city_id","left")
            ->join("departments AS d","d.id" ,"=", "c.department_id","left")
            ->join("countries AS co","co.code" ,"=", "d.country_code","left")
            ->where("u.id", $id)
            ->where('u.deleted',0);

        return $query->first();
    }

    public static function getCities($id = null){
        
        $query = DB::table("cities as c")->select("c.*", "d.name as name_d", "co.name as country")
            ->join("departments AS d","d.id" ,"=", "c.department_id")
            ->join("countries AS co","co.code" ,"=", "d.country_code")
            ->where('c.deleted',0);
        
        if(!empty($id)){
            $query = $query->where("c.id",$id);
            return $query->first();

        }

        return $query->get();

    }
    
}
