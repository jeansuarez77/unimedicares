<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $connection = 'mysql';
    protected $table  = 'options';
    protected $hidden = [
        'created_at', 'updated_at'
    ];  
    public $timestamps = false;

    protected $fillable = ["question_id","enunciate","deleted","order"];

}

?>
