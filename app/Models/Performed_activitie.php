<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use  DB;

class Performed_activitie extends Model
{
    use HasFactory;

    public $timestamps = false;

    public static function getQuantityTest($course_id)
    {
        
        $num_test = DB::table('levels as n')
                        ->join("lessons AS le","le.level_id" ,"=", "n.id")
                        ->join("activities AS a","a.lesson_id" ,"=", "le.id")
                        ->where("course_id",$course_id)
                        ->where("le.deleted",0)
                        ->where("n.deleted",0)
                        ->where("a.deleted",0)
                        ->where('a.type',3)
                        ->count();

        return $num_test;

    }

    // cantidad de test realizados por estudiante
    public static function getQuantityTestStudent($course_id, $id_user)
    {
        
        $num_test = DB::table('performed_activities as p')
                    ->join("activities AS a","a.id" ,"=", "p.activity_id")
                    ->where("p.course_id",$course_id)
                    ->where("p.user_id",$id_user)
                    ->where("a.deleted",0)
                    ->where('a.type',3)
                    ->count();
                    
        return $num_test;

    }
}
