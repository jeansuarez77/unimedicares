<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $connection = 'mysql';
    protected $table  = 'lessons';
    protected $hidden = [
        'created_at', 'updated_at'
    ];
    protected $fillable = ["level_id","description","deleted","order"];

}

?>
