<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class TestQuestion extends Model
{
    protected $connection = 'mysql';
    protected $table  = 'test_questions';
    protected $hidden = [
        'created_at', 'updated_at'
    ];  
    public $timestamps = false;

    protected $fillable = ["question_id","activity_id","deleted"];


    public static function getInfoTest($key, $activity_id, $type = 1)
    {
        
        $info = DB::table("test_questions as tq")
                ->select(["q.questions","q.answer_id","q.id"])
                ->join("questions AS q","q.id" ,"=", "tq.question_id");
                
        if($type == 2){
            $info = DB::table("test_questions as tq")
                ->select(["q.questions","q.answer_id","q.id", "o.id as option_id", "enunciate"])
                ->join("questions AS q","q.id" ,"=", "tq.question_id")
                ->join("options AS o","q.id" ,"=", "o.question_id")
                ->where("tq.activity_id",$activity_id)
                ->where("q.id",$key)
                ->where("o.deleted",0)
                ->orderBy("o.order","asc")
                ->get();

        }else{
            $info = DB::table("test_questions as tq")
                ->select(["q.questions","q.answer_id","q.id"])
                ->join("questions AS q","q.id" ,"=", "tq.question_id")
                ->where("tq.activity_id",$activity_id)
                ->where("q.id",$key)
                ->first();

        }
                    
        return $info;

    }

}

?>
