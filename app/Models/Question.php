<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $connection = 'mysql';
    protected $table  = 'questions';
    protected $hidden = [
        'created_at', 'updated_at'
    ];  
   public $timestamps = false;

    protected $fillable = ["questions","answer_id","created_by","deleted","area_id","type"];

}

?>
