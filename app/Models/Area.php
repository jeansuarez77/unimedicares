<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $connection = 'mysql';
    protected $table  = 'areas';
    protected $hidden = [
        'created_at', 'updated_at'
    ];  
   // public $timestamps = false;

    protected $fillable = ["name","description","img_url","deleted"];

}

?>
