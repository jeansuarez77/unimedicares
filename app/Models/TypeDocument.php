<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeDocument extends Model
{
    protected $connection = 'mysql';
    protected $table  = 'types_documents';
    protected $hidden = [
        'created_at', 'updated_at'
    ];  
    public $timestamps = false;

    protected $fillable = ["code","name","deleted"];

}

?>
