<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $connection = 'mysql';
    protected $table  = 'courses';
    protected $hidden = [
        'created_at', 'updated_at'
    ];  
   // public $timestamps = false;

    protected $fillable = ["area_id","name","code","url","description","img_vide","price","deleted","is_free","intensity","validity"];
}
