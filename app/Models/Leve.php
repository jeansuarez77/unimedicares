<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Leve extends Model
{
    protected $connection = 'mysql';
    protected $table  = 'levels';
    protected $hidden = [
        'created_at', 'updated_at'
    ];  
   // public $timestamps = false;

    protected $fillable = ["course_id","description","deleted","order"];

}

?>
