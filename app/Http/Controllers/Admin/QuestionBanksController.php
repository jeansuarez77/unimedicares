<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use App\Models\Option;
use App\Models\Question;
use App\Models\TestQuestion;
use App\Models\Area;

class QuestionBanksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //  $this->middleware('auth');
    }

    
    public function index()
    {

        $search = trim(request("search") ? request("search"): "");  
        $area_id = request("area_id");        
        $query = DB::table("questions as q")->where("q.deleted",0)
        ->select(["q.id","q.questions","a.name as area","q.type","u.name","u.surname"])
        ->leftJoin ("areas AS a","a.id" ,"=", "q.area_id")
        ->leftJoin ("users AS u","u.id" ,"=", "q.created_by");        

        if(!empty($search)){
            $query->where(function($query) use ($search)
            {		 	
                $query->where('q.questions', 'like', "%".$search."%");  
                
            });
        }
        if($area_id > 0){
            $query->where("q.area_id",$area_id);
        }

       
        $data['search'] = $search;
        $data['area_id'] = $area_id;
        $data['areas'] = Area::select(["id","name"])->where("deleted",0)->orderBy("name")->get();
        $data["questions"] = $query->paginate()->appends(request()->query());
        return view('question/list',$data);
    }

    public function view(Request $req, $id = -1)
    {
        $data = array(
            "question_id" => $id,
            "question" =>Question::findOrNew($id)
        );
        
        return view('question/create-edit',$data);
    }

   
    function delete_question(Request $req,$question_id){
        $resp = Question::where("id",$question_id)->update(["deleted"=>1]);
        return response()->json(["success" =>(bool)$resp ],200); 
    }

    function get_question(Request $req, $id){
        $question = DB::table("questions as q")->where("q.id", $id)->first();
        $options = DB::table("options")->where("question_id", $id)->where("deleted",0)->orderBy("order","asc")->get();
        
        $data["question"] = $question;
        $data["options"] = $options;
        return response()->json( $data,200);
    }

    function searcher_question(Request $req){
        $searcher = $req->searcher;
        $area_id = $req->area_id;
        $data["question"] = DB::table("questions as q")->select(["questions","id"])->where("deleted",0)
        ->where("area_id",$area_id)
        ->where('questions', 'like', "%$searcher%")->limit(50)->get();
        return response()->json( $data,200);       
    } 
    function get_option(Request $req,$id){
        
        $data["options"] = DB::table("options as q")->select(["enunciate","id"])->where("deleted",0)->orderBy("order","asc")
        ->where("question_id",$id)->get();
        return response()->json( $data,200);       
    }    

    function create_question(Request $req, $id){
        $validate = [
            'questions' => 'required|max:255',
            'area_id' => "required",
            'type' => "required",
        ];
        $req->validate($validate);
        $dataQ = [
            "questions" => $req->questions,
            'area_id' => $req->area_id,
            'type' => (int)$req->type,
           // "created_by" => auth()->user()->id,           
        ]; 
        if($id <= 0){
            $dataQ["created_by"] = auth()->user()->id;          
        }
        $options = array();
        $options_data  = $req->options;
        if($options_data["option1"] ){
            $option = $options_data["option1"];
            if(empty($option["enunciate"])){
                return response()->json(["success" =>false, "id"=>$id],500);
            }
            $options[1] = array(
                "id" => $option["id"],
                "enunciate" =>$option["enunciate"]
            );
        }
        if($options_data["option2"] ){
            $option = $options_data["option2"];
            if(!empty($option["enunciate"])){               
                $options[2] = array(
                    "id" => $option["id"],
                    "enunciate" =>$option["enunciate"]
                );
            }
        }
        if($options_data["option3"] ){
            $option = $options_data["option3"];
            if(!empty($option["enunciate"])){               
                $options[3] = array(
                    "id" => $option["id"],
                    "enunciate" =>$option["enunciate"]
                );
            }
        }
        if($options_data["option4"] ){
            $option = $options_data["option4"];
            if(!empty($option["enunciate"])){               
                $options[4] = array(
                    "id" => $option["id"],
                    "enunciate" =>$option["enunciate"]
                );
            }
        }
        if($options_data["option5"] ){
            $option = $options_data["option5"];
            if(!empty($option["enunciate"])){               
                $options[4] = array(
                    "id" => $option["id"],
                    "enunciate" =>$option["enunciate"]
                );
            }
        }
        if($options_data["option6"] ){
            $option = $options_data["option6"];
            if(!empty($option["enunciate"])){               
                $options[4] = array(
                    "id" => $option["id"],
                    "enunciate" =>$option["enunciate"]
                );
            }
        }
       
        $resp = false;
        DB::beginTransaction();
        try {
            $resp = true;
            if($id > 0){
                 Question::where("id",$id)->update($dataQ);
            }else{ 
                $question = Question::create($dataQ);
                $id = $question->id;
                //$resp = $question->exists;
            }

            
           // if($resp){
                /*$exists = TestQuestion::where('activity_id', $activity_id)->where('question_id', $id)->exists();
                if ($exists) {
                    TestQuestion::where("id",$id)->update(["deleted"=>0]);
                }else{
                    TestQuestion::create([
                        "question_id" =>$id,
                        "activity_id" => $activity_id,
                        "deleted" => 0
                    ]);

                }*/

                Option::where("question_id",$id)->update(["deleted"=>1]);
                $ids =[];
                $order = 1;
                foreach ($options as $key => $option) {
                    $idOP = $option["id"];
                    $dataOP = array(
                        "question_id" => $id,
                        "enunciate" => $option["enunciate"],
                        "deleted" => 0,
                        "order" => $order
                    );
                    $order ++;
                    if($option["id"] > 0){
                        Option::where("id",$idOP)->update($dataOP);
                    }else{ 
                        $option = Option::create($dataOP);  
                        $idOP = $option->id;                                  
                    } 
                    if($req->type == 1){
                        if($key == 1){
                            Question::where("id",$id)->update(["answer_id" => $idOP]); 
                        }
                    }
                }
           // }
            DB::commit(); 
        }catch (\Exception  $e) {
            DB::rollBack();            
            $resp = false;
         }
        if($resp)
            return response()->json(["success" =>true, "id"=>$id],200);
        return response()->json(["success" =>false ],200);
    }

   /* function get_test_questions(Request $req, $activity_id){
        $questions = DB::table("test_questions as tq")
        ->select(["q.questions","q.answer_id","id"])
        ->join("questions AS q","q.id" ,"=", "tq.question_id")
        ->where("tq.deleted",0)->where("q.deleted",0)->where("tq.activity_id",$activity_id)->get()->toArray();
        
        foreach ($questions as $key => $question) {
            $questions[$key]->options=
            Option::select(["enunciate","id"])->where("deleted",0)->where("question_id",$question->id)->inRandomOrder()->get(); 
        }

        return response()->json(["questions" =>$questions],200);
    }*/

    function get_areas(){
        $areas = Area::select(["id","name"])->where("deleted",0)->get();
        return response()->json(["areas" =>$areas],200);
    }
}
