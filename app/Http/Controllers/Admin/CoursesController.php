<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use Storage;
use App\Models\Area;
use App\Models\Course;
use App\Models\Leve;
use App\Models\Lesson;
use App\Models\Activity;
use App\Models\Option;
use App\Models\Question;
use App\Models\TestQuestion;

class CoursesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //  $this->middleware('auth');
    }

    private function slug($string)
    {
        return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
    }

    
    public function index()
    {
        $search = trim(request("search") ? request("search"): "");  
        $area_id = request("area_id");        
        $query =  DB::table("courses as c")
        ->select(["c.name","c.id","a.name as area", "price","c.code","is_free"]);        

        if(!empty($search)){
            $query->where(function($query) use ($search)
            {		 	
                $query->where('c.name', 'like', "%".$search."%");  
                $query->orWhere('c.code', 'like', "%".$search."%");  
            });
        }
        if($area_id > 0){
            $query->where("c.area_id",$area_id);
        }
        $query->join("areas AS a","a.id" ,"=", "c.area_id")
        ->where("c.deleted",0);

        $data["courses"] = $query->paginate()->appends(request()->query());
        $data['search'] = $search;
        $data['area_id'] = $area_id;
        $data['areas'] = Area::select(["id","name"])->where("deleted",0)->orderBy("name")->get();
       
        return view('courses/list',$data);
    }

    public function areas()
    {

        $search = trim(request("search") ? request("search"): "");  
        $query = DB::table("areas as a");

        if(!empty($search)){
            $query->where(function($query) use ($search)
            {		 	
                $query->where('a.name', 'like', "%".$search."%");  
                 
            });
        }
        $data['search'] = $search;
        $data["areas"] = $query->select(["a.name","a.id","a.name","a.description","img_url"])
        ->where("a.deleted",0)->paginate()->appends(request()->query());
        return view('courses/area-list',$data);
    }
    public function get_area($id = -1){
        $area = Area::find($id);
        return response()->json(["area" =>$area ],200);
    }
    public function create_area(Request $request, $id = -1)
    {
        $area = null;
        $data = array(
            "name" => $request->name,
            "description" => $request->description,
            "deleted" => 0
        );       
        
        if($request->hasFile("logo")){
            $dir = "/areas/logos/";
            $path = public_path();
            if($id > 0){
                $area = Area::find($id);
                if($area->img_url != null){
                    \File::delete( $path .$area->img_url);
                    //Storage::disk('public')->delete('URL'.$ARCHIVO_A_ELIMINAR);
                }
            }
            $file = $request->file("logo");           
            
            $fileName = uniqid() .".". $file->getClientOriginalExtension();        
            $file->move($path.$dir, $fileName);            
            $data["img_url"] =  $dir.$fileName;
        }
        if($id > 0){
            $resp = Area::where("id",$id)->update($data);
        }else{
            $resp = Area::create($data);
            $resp = $resp->exists;
        }
        return response()->json(["success" =>$resp ],200);       
        
    }

    function delete_area(Request $request, $id){
        $resp = Area::where("id",$id)->update(["deleted"=>1]);
        return response()->json(["success" =>(bool)$resp ],200);    
    }
    function delete_course(Request $request, $id){
        $resp = Course::where("id",$id)->update(["deleted"=>1]);
        return response()->json(["success" =>(bool)$resp ],200);    
    }
    function courses_view(Request $request, $id = -1){
       $couerse = Course::findOrNew($id);
        
        $data = [
            "areas" =>Area::select(["id","name"])->where("deleted",0)->orderBy("name")->get(),
            "couerse" => $couerse,
            "course_id" =>$id
        ];
        
        return view('courses/form-courses',$data);
         
    }
    function delete_leve(Request $req, $id){
        $resp = Leve::where("id",$id)->update(["deleted"=> 1]);
        if($resp)
            return response()->json(["success" =>true ],200);
        return response()->json(["success" =>false ],200);
    }

    function delete_lesson(Request $req, $id){
        $resp = Lesson::where("id",$id)->update(["deleted"=> 1]);
        if($resp)
            return response()->json(["success" =>true ],200);
        return response()->json(["success" =>false ],200);
    }
    
    function save_course(Request $req, $id = -1){
        $validate = [
            'name' => 'required|max:255',
            'code' => 'required|max:20|unique:courses,code,'.$id,
            'description' => 'required',
            'area_id' => 'required',
            "intensity" =>'required',
            "validity" =>'required'
        ];

        if($id < 1 or $req->hasFile("logo")){
            $validate['logo'] = 'required|max:8192|mimes:jpeg,png,jpg,webp'; // 8mb
        }

        if($id < 1 or $req->hasFile("file1")){
            $validate['file1'] = 'required|max:102400|mimes:mp4,wmv,avi';//100 mg
        }

        $req->validate($validate);

        $data = [
            "name" => $req->name,
            "area_id" => $req->area_id,
            "code"=> $req->code,
           // "url"=> $req->url,
            "description"=> $req->description,            
            "price" => (double)$req->price,
            "is_free" =>(int)$req->is_free,
            "intensity" =>(int)$req->intensity,
            "validity" =>(int)$req->validity,
            "deleted" => 0
        ];    
        $course = null;   
        if($id > 0){
            $resp = Course::where("id",$id)->update($data);
        }else{
            $course = Course::create($data);
            $id = $course->id;
            $resp = $course->exists;
        }

        if($req->hasFile("logo")){
            $dir = "/courses/logos/".$id."/";
            $path = public_path();
           
            $course =  $course == null?  Course::find($id): $course;
            if($course->logo != null){
                \File::delete( $path .$course->logo);
                //Storage::disk('public')->delete('URL'.$ARCHIVO_A_ELIMINAR);
            }
            $file = $req->file("logo");
            $fileName = uniqid() .".". $file->getClientOriginalExtension();        
            $file->move($path.$dir, $fileName);
            Course::where("id",$id)->update(["logo"=>$dir.$fileName]);
        }
        if($req->hasFile("file1")){
            $dir = "/courses/videos/".$id."/";
            $path = public_path();
           
            $course =  $course == null?  Course::find($id): $course;
            if($course->file1 != null){
                \File::delete( $path .$course->file1);
                //Storage::disk('public')->delete('URL'.$ARCHIVO_A_ELIMINAR);
            }
            $file = $req->file("file1");
            $fileName = uniqid() .".". $file->getClientOriginalExtension();        
            $file->move($path.$dir, $fileName);
            Course::where("id",$id)->update(["img_vide"=>$dir.$fileName]);
        }
        // si usa una version de mysql mayor a la 5  usar    
        //  Course::whereNull("url")->update(['url' => DB::raw("to_url(name, id)")]);
        // esta genera las url mas limpias 
        
        $cus = Course::whereNull("url")->where("deleted",0)->get();
        foreach($cus as $key => $c){
            $url = $this->slug($c->name);
            Course::where("id",$c->id)->update(["url"=>$url."-". $c->id]);
        }
       
        if($resp)
            return redirect('admin/courses');
        else
            return redirect("admin/courses/courses-view/$id");
        
    }

    function courses_manage($id){
        $data = [
            "course" =>Course::select(["id","name","area_id"])->first(),
            "course_id" => $id
        ];
        return view('courses/manage-course',$data);
    }
    function get_levels($course_id){
        $levels = Leve::where("deleted",0)->where("course_id",$course_id)->orderBy("order")->get()->toArray();
        foreach($levels as $key =>$level){
            $lessons = Lesson::where("level_id",$level["id"])->where("deleted",0)->orderBy("order")->get()->toArray();
            foreach($lessons as $key2 =>$less){
                $activity = Activity::where("lesson_id",$less["id"])->where("deleted",0)->orderBy("order")->get()->toArray();;
                $lessons[$key2]["activities"] =  $activity;
            }
            $levels[$key]["lessons"] = $lessons;
        }
        $data = [
            "levels" =>$levels
        ];

        return response()->json($data,200);
       
    }

    function create_level(Request $req,$id){
        $latest = Leve::select("order")->where("course_id",$id)->where("deleted",0)->orderBy("order","desc")->first();
        $order = 1;
        if($latest){
            $order = $latest->order +1;
        }
        $resp = Leve::create([
            "course_id" =>$id,
            "description" => "",
            "deleted"=> 0,
            "order" =>$order
        ]);

        if($resp)
            return response()->json(["success" =>true ],200);
        return response()->json(["success" =>false ],200);
    }

    function create_lesson(Request $req, $id){
        $latest = Lesson::select("order")->where("level_id",$id)->where("deleted",0)->orderBy("order","desc")->first();
        $order = 1;
        if($latest){
            $order = $latest->order +1;
        }
        $resp = Lesson::create([
            "level_id" =>$id,
            "description" => "",
            "deleted"=> 0,
            "order" =>$order
        ]);

        if($resp)
            return response()->json(["success" =>true ],200);
        return response()->json(["success" =>false ],200);
    }
    function get_activity(Request $req, $activity_id, $lesson_id){
        $data["activity"] = Activity::where("id",$activity_id)->where("lesson_id",$lesson_id)->first();
        return response()->json($data,200);
    }

    function delete_activity(Request $req, $activity_id, $lesson_id){
        $resp = Activity::where("id",$activity_id)->where("lesson_id",$lesson_id)->update(["deleted"=>1]);
        if($resp){
            $activity = Activity::find($activity_id);
            if($activity->file_url != null){                
                $path = public_path();
                \File::delete( $path .$activity->file_url);
            }
        }
        return response()->json(["success" =>(bool)$resp ],200); 
    }
    function delete_question(Request $req,$question_id, $activity_id){
        //$resp = Question::where("id",$question_id)->update(["deleted"=>1]);
        $resp = TestQuestion::where("activity_id",$activity_id)->where("question_id",$question_id)->update(["deleted"=>1]);
        return response()->json(["success" =>(bool)$resp ],200); 
    }
    function create_activity(Request $req, $id){
        $validate = [
            'name' => 'required|max:255', 
            'description' => 'required',
            'lesson_id' => 'required',            
        ];        

        if(($id < 0 and $req->type == 2 ) or ($req->type == 2  and $req->hasFile("file"))){ // infografia
            $validate['file'] = 'required|max:8192|mimes:jpeg,png,jpg,webp,pdf'; // 8mb
        }

        if(($id < 0 and $req->type == 1 )  or ($req->type == 1 and $req->hasFile("file"))){ // videos
            $validate['file'] = 'required|max:102400|mimes:mp4,wmv,avi';//100 mg
        }

        if(($id < 0 and $req->type == 4 ) or ($req->type == 4  and $req->hasFile("file"))){ // audios
            $validate['file'] = 'required|max:8192|mimes:mp3,wav'; // 8mb
        }

        $req->validate($validate);
        

        $data = [
            "lesson_id" => $req->lesson_id,
            "name" => $req->name,
            "type"=> $req->type,                   
            "description" => $req->description,          
            "deleted" => 0
        ];    
        if($req->type == 3){
            $data["number_questions"] = $req->number_questions > 0 ? $req->number_questions: null;
            $data["trial_numbers"] = $req->trial_numbers ?  $req->trial_numbers : null;
            $data["porcent_acceptance"] = $req->porcent_acceptance;
        }

        $activity = null;   
        if($id > 0){
            $resp = Activity::where("id",$id)->update($data);
        }else{
            $latest = Activity::select("order")->where("lesson_id",$req->lesson_id)->where("deleted",0)->orderBy("order","desc")->first();
            $order = 1;
            if($latest){
                $order = ((int)$latest->order) +1;
            }
            $data["order"] = $order;
            $activity = Activity::create($data);
            $id = $activity->id;
            $resp = $activity->exists;
        }

        if($req->hasFile("file")){
            $dir = "/courses/resources/".$id."/";
            $path = public_path();
           
            $activity =  $activity == null?  Activity::find($id): $activity;
            if($activity->file_url != null){
                \File::delete( $path .$activity->file_url);
                //Storage::disk('public')->delete('URL'.$ARCHIVO_A_ELIMINAR);
            }
            $file = $req->file("file");
            $fileName = uniqid() .".". $file->getClientOriginalExtension();        
            $file->move($path.$dir, $fileName);
            Activity::where("id",$id)->update(["file_url"=>$dir.$fileName]);
        }
       
        if($resp)
            return response()->json(["success" =>true, "id"=>$id],200);
        return response()->json(["success" =>false ],200);
    }

    function add_question(Request $req){
       $activity_id =  $req->activity_id;
       $question_id =  $req->question_id;
       
            
        $exists = TestQuestion::where('activity_id', $activity_id)->where('question_id', $question_id)->exists();
        if ($exists) {
            TestQuestion::where('activity_id', $activity_id)->where('question_id', $question_id)->update(["deleted"=>0]);
        }else{
            TestQuestion::create([
                "question_id" =>$question_id,
                "activity_id" => $activity_id,
                "deleted" => 0
            ]);

        }
       
        return response()->json(["success" =>true, "id"],200);
      
    }

    function get_test_questions(Request $req, $activity_id){
        $questions = DB::table("test_questions as tq")
        ->select(["q.questions","q.answer_id","id","type"])
        ->join("questions AS q","q.id" ,"=", "tq.question_id")
        ->where("tq.deleted",0)->where("q.deleted",0)->where("tq.activity_id",$activity_id)->get()->toArray();
        
        foreach ($questions as $key => $question) {
            $questions[$key]->options=
            Option::select(["enunciate","id"])->where("deleted",0)->where("question_id",$question->id)->inRandomOrder()->get(); 
        }

        return response()->json(["questions" =>$questions],200);
    }
}
