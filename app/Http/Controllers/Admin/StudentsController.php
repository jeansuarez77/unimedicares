<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\User;
use App\Models\TypeDocument;
class StudentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
      $search = trim(request("search") ? request("search"): "");  
      $query =   DB::table("users as u");        

      if(!empty($search)){
         $query->where(function($query) use ($search)
         {		 	
            $query->where('u.email', 'like', "%".$search."%");  
            $query->orWhere('u.document', 'like', "%".$search."%"); 
            $query->orWhere(DB::raw("CONCAT(u.name, ' ', u.surname, ' ', u.second_surname)"), 'LIKE', "%".$search."%"); 
         });
      }
     
      $data['search'] = $search;
      $data["students"] = $query->where("u.rol","!=","admin")->where("u.deleted",0)->paginate()->appends(request()->query());
      return view('users/students',$data);
    }

    function generate($student_id,$course_id){

      $meses = array(
         "01"=>"ENERO",
         "02"=>"FEBRERO",
         "03"=>"MARZO",
         "04"=>"ABRIL",
         "05"=>"MAYO",
         "06"=>"JUNIO",
         "07"=>"JULIO",
         "08"=>"AGOSTO",
         "09"=>"SEPTIEMBRE",
         "10"=>"OCTUBRE",
         "11"=>"NOVIEMBRE",
         "12"=>"DICIEMBRE"
      );
      $msj = '<h1 style="color:red" >Certificado no disponible</h1>';      

      $user = DB::table("users AS e")
      ->select(["ci.name as document_city","e.name","surname","second_surname","document","document_type_id","tp.code as document_code","ci_u.name as city_user"])
      ->leftJoin("types_documents AS tp","tp.id" ,"=", "e.document_type_id")
      ->leftJoin("cities AS ci","ci.id" ,"=", "e.document_city_id")
      ->leftJoin("cities AS ci_u","ci_u.id" ,"=", "e.city_id")
      ->where("e.id", $student_id)->first();

      if(!User::is_data_valid($user)){
         $course = DB::table("courses_students as cs")
         ->select('c.id','c.name', 'c.code', 'cs.state', 'a.name as area',"approved","end_date","intensity","cs.created_at","end_date","cs.validity")
         ->join("courses AS c","c.id" ,"=", "cs.course_id")
         ->join("areas AS a","a.id" ,"=", "c.area_id")
         ->where('user_id',$student_id)->where('course_id',$course_id)->where('cs.deleted',0)->first(); 

         if($course and $course->state == 0 and $course->approved == 1){
         
            $end_date = $course->end_date;
            $end_start = $course->created_at;
            $y1 = date('Y', strtotime($end_start));
            $m1 = date('m', strtotime($end_start));
            $d1 = date('d', strtotime($end_start));

            $y2 = date('Y', strtotime($end_date));
            $m2 = date('m', strtotime($end_date));
            $d2 = date('d', strtotime($end_date));

            $validity =  (int)$course->validity;
            $validity =  date("d-m-Y",strtotime($end_date ."+ $validity days"));
            $y3 = date('Y', strtotime($validity));
            $m3 = date('m', strtotime($validity));
            $d3 = date('d', strtotime($validity));

            $y4 = date('Y',);
            $m4 = date('m');
            $d4 = date('d');
            $data = [
               "name" => strtoupper($user->name." ".$user->surname." ".$user->second_surname),
               "document_type" => $user->document_code,
               "document" => $user->document,
               "document_from" => strtoupper($user->document_city),
               "course_name" =>strtoupper($course->name),
               "relize_in" =>"Realizado los dias $d1 al $d2 de ". strtolower($meses[$m2]). " de $y2, con una intensidad de {$course->intensity} horas ",
               "vigencia" => "CERTIFICACION VIGENTE HASTA EL DÍA $d3 ".$meses[$m3]." DE $y3",
               "intensity" => $course->intensity,
               "given_in" => "DADO EN ". strtoupper($user->city_user).", A LOS  $d4 DÍAS DEL MES DE ".$meses[$m4]." $y4"
            ];
            //return view('certificates.templates.template-01',$data);
            $pdf = \PDF::loadView('certificates.templates.template-01',$data);
            $pdf->setPaper( array(0,0,750,450)); //x inicio, y inicio, ancho final, alto final
            return $pdf->download('certificado.pdf');
         }
      }else{
         $msj = '<h1 style="color:red" >Debe completar los datos personales</h1>';
      }

      $pdf = app('dompdf.wrapper');
      $pdf->setPaper( array(0,0,400,200)); 
      $pdf->loadHTML($msj);
      return $pdf->download('certificado-error.pdf');
    }

    public function profile(){
      $user = auth()->user();     
      $data["cities"] = User::getCities();

      $id_user = $user->id;
      $data["courses"] = DB::table("courses_students as cs")
               ->select('c.id',
               'c.name', 
               'c.code', 
               'c.logo', 
               'c.url', 
               'cs.state',
               'a.name as area',
               'cs.created_at as date_start',
               "approved", "is_free", 
               "price", 
               "is_payment_finished",
               "purchase_price",
               "progress")
               ->join("courses AS c","c.id" ,"=", "cs.course_id")
               ->join("areas AS a","a.id" ,"=", "c.area_id")
               ->where('user_id',$id_user)->where('cs.deleted',0)->orderBy("cs.created_at","desc")->get();

      $data["user_id"] = $id_user;
      $data["is_data_valid"] = User::is_data_valid($user);
     // $data["type_tocuments"] = TypeDocument::where("deleted",0)->get();
      
      $data["user"] = User::getInfoUser($id_user);
        

        return view('users/profile',$data);
    }
}
