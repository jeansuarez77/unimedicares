<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function showRegistrationForm()
    {
        $cities = User::getCities();
        $types_documents = DB::table("types_documents")->where('deleted',0)->get();

        return view('auth.register', compact('cities','types_documents'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:150'],
            'surname' => ['required', 'string', 'max:150'],
            'second_surname' => ['required', 'string', 'max:150'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'document_type_id' => ['required'],
            'document' => ['required', 'string', 'min:8','max:50'],
            'city_id' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {

        $city = User::getCities($data['city_id']);

        return User::create([
            'document_type_id' => $data['document_type_id'],
            'document' => $data['document'],
            'name' => $data['name'],
            'email' => $data['email'],
            'surname' => $data['surname'],
            'second_surname' => $data['second_surname'],
            'phone' => $data['phone'],
            'department_id' =>  $city->department_id,
            'city_id' =>  $city->id,
            'country_code' =>  $city->country_code,
            'password' => Hash::make($data['password']),
        ]);
    }
}
