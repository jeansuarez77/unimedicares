<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Input;
use App\Models\Courses_student;
use App\Models\Course;

use Auth;

class PayController extends Controller
{
    //

    public function pay_course($id){

        $id_user = Auth::user()->id;

        $course = Course::find($id);
        $Courses_student = Courses_student::where('course_id',$course->id)->where('user_id',$id_user)->where('deleted',0)->first();
        $referenceCode = $Courses_student->id.'_'.strtotime("now");
        $data = [
            "merchantId" => env('MERCHANTID'),
            'accountId' => 963790,
            'description' => 'CURSO '.$course->name,
            'referenceCode' => $referenceCode,
            'amount' => $Courses_student->purchase_price > $course->price ? $course->price : $Courses_student->purchase_price,
            'signature' => md5(env('APIKEY').'~'.env('MERCHANTID').'~'.$referenceCode.'~'.$Courses_student->purchase_price.'~COP'),
            'buyerEmail' => Auth::user()->email,
            'responseUrl' => route('transaction.pay'),
        ];
        
        return response()->json($data, 200);

    }

    public function processTransaction(){
        $referenceCode = request('referenceCode');

        $id_user = Auth::user()->id;
        $Courses_student = Courses_student::where('id',$referenceCode)->where('user_id',$id_user)->where('deleted',0)->first();

        $estadoTx = '';
        $is_payment_finished = 0;
        if (request('transactionState') == 4 ) {
            $estadoTx = "Transacción aprobada";
            $is_payment_finished = 1;
        }
        
        else if (request('transactionState') == 6 ) {
            $estadoTx = "Transacción rechazada";
        }

        if(!empty($Courses_student->id)){
            $Courses_student->transactionId = request("transactionId");
            $Courses_student->date_transaction = request("processingDate");
            $Courses_student->transactionState = $estadoTx;
            $Courses_student->is_payment_finished = $is_payment_finished;
            $Courses_student->save();
        }


        return redirect('students/profile');


    }

    
}
