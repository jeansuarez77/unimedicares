<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use DB;
use Hash;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function list(Request $req){
        $data = [];
        $search = trim(request("search") ? request("search"): "");  
        $query =   DB::table("users as u");        

        if(!empty($search)){
            $query->where(function($query) use ($search)
            {		 	
                $query->where('u.email', 'like', "%".$search."%");  
                $query->orWhere('u.document', 'like', "%".$search."%"); 
                $query->orWhere(DB::raw("CONCAT(u.name, ' ', u.surname, ' ', u.second_surname)"), 'LIKE', "%".$search."%"); 
            });
        }
        
        $data['search'] = $search;
        $data["users"] = $query->where("u.rol","=","admin")->paginate()->appends(request()->query());
        return view('users/admin',$data);

    }
    public function delete_user(Request $req, $id){
        if( $id  != 1)
           User::where("id",$id)->update(["deleted"=> 1]);
           
        return response()->json(["success" =>true ,"message" => "Ususrio elimnaod"],200);

    }
    public function change_password(Request $req)
    {
        $validate = [
            'password_current' => ['required', 'string', 'max:150'],          
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];

        $user = auth()->user();
        $password_current  = $req->password_current;
        $pass1 = $user->password;
        $req->validate($validate);
        if(\Hash::check($password_current,$pass1)){
            $req->validate($validate);
            $resp = User::where("id",$user->id)->update(["password"=> \Hash::make( $password_current)]);
            if($resp)
                return response()->json(["success" =>true ,"message" => "Contraseña actualizada"],200);
            return response()->json(["success" =>false ,"message" => "Error al actualizar contraseña"],200);

        }else{
            return response()->json(["errors" =>["password_current" =>["contraseña actual incorrecta"]]],422);
             
        }

    }
    function get_user(Request $req , $id){
        $data["user"]=  User::where("id",$id)->first();
        return response()->json($data,200);
    }
    public function create_user(Request $req, $id = -1){

        $validate = [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email,'.$id,
            'surname' => 'required',
            'second_surname' => 'required'
        ];
        $req->validate($validate);
        $data = [
            'name' => $req->name,
            'email' => $req->email,
            'surname' => $req->surname,
            'second_surname' =>$req->second_surname,            
            "rol" =>"admin"
        ];
        if(!empty($req->password)){
            $data["password"] = Hash::make($req->password);
        }
        if($id < 0 and empty($req->password))
            $data["password"] = Hash::make(time());

        if($id < 0)
            $user =  User::create( $data);
        else {
            $user = User::where("id",$id)->update( $data);
        }
        if($user){
            return response()->json(["success" =>true ,"message" => "Ususrio guardado"],200);
        }else{
            return response()->json(["success" =>false ,"message" => "Erro al guardar"],200);
        }
    }
    public function update_info(Request $req){

        $validate = [
            'name' => ['required', 'string',  'min:3', 'max:150'],  
            'surname' => ['required', 'string', 'min:3', 'max:150'], 
            'second_surname' => ['required', 'string', 'max:150'],  
        ];

        $user = auth()->user();
        $info = User::find($user->id);
        $req->validate($validate);
        
        if($info){

            $info->name = $req->name;
            $info->surname = $req->surname;
            $info->second_surname = $req->second_surname;
            $info->phone = $req->phone;
            $info->address = $req->address;
            $info->city_id = $req->city_id;
            $info->document_city_id = $req->document_city_id;

            if($info->save())
                return response()->json(["success" =>true ,"message" => "Información Actualizada"],200);
            return response()->json(["success" =>false ,"message" => "Error al actualizar la información"],200);

        }else{
            return response()->json(["errors" =>["password_current" =>["No fue posible encontrar el usuario"]]],422);
             
        }

    }
    
}
