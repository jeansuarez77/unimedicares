<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Courses_student;
use App\Models\Option;
use App\Models\Activity;
use App\Models\TestQuestion;

use App\Models\Performed_activitie;
use Illuminate\Http\Request;

use DB;
use Auth;
use View;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
    }

    public function index()
    {
        //
        $data['areas'] = DB::table("areas")->where('deleted',0)->get();
        return view('public.courses.home',$data);
    }

    public function get_courses_area($area){

        $data['courses'] = DB::table("courses")->where('deleted',0)->where('area_id',$area)->get();
        return response()->json($data,200);
    }

    public function getCourseDetails($url, $activity_id = null){


        if(!empty($url)){
            $course = DB::table("courses")->where('url',$url)->where('deleted',0)->first();

            if($course == null)
                return redirect()->back();

            
            $my_course_info = [];
            $id_user = null;
            $test_f = 0;
            $num_test = 0;

            // verificamos si el usuario esta logueado
            if(Auth::check()){
                $id_user = $id_user = Auth::user()->id;
                $my_course_info = DB::table("courses_students")->where('course_id',$course->id)->where('user_id',$id_user)->where('deleted',0)->first();

                $num_test = Performed_activitie::getQuantityTest($course->id);

                $test_f = Performed_activitie::getQuantityTestStudent($course->id, $id_user); 
            }

            // verificamos que este seleccionada una actividad
            $info_activity = null;
            if($activity_id && $id_user){

                $info_activity = Activity::getInfoActivity($activity_id);
                $course = DB::table("courses")->where('url',$url)->where('deleted',0)->first();
                

                $performed = DB::table("performed_activities")->where('user_id',$id_user)->where('activity_id',$activity_id)->where('deleted',0)->first();
                if(!$performed and  $info_activity->type != 3){
                    
                    $performed = new Performed_activitie;
                    $performed->user_id = $id_user;
                    $performed->activity_id = $activity_id;
                    $performed->show = 1;
                    $performed->course_id = $course->id;
                    $performed->save();
                }

                

            }
            
            // obtenemos todos los niveles, lecciones y actividades del curso
            $levels = DB::table("levels")->where('course_id',$course->id)->orderBy("order","asc")->get();
            
            foreach ($levels as $key=> $level) {
                $lessons = DB::table("lessons")->where('level_id',$level->id)->orderBy("order","asc")->where('deleted',0)->get();

                foreach($lessons as $key2 => $lesson){

                    $query =  DB::table("activities as a");
                                           
                    if(!empty($id_user)){
                        $query = $query->select('a.*','pa.show as seen')
                        ->leftJoin('performed_activities as pa', function ($join) use($id_user) {
                            $join->on('pa.activity_id', '=', 'a.id')
                                ->where('pa.user_id', '=', $id_user);
                        });
                    }else{
                        $query = $query->select('a.*', DB::raw('0 as seen'));
                    }
                    $lesson->activities = $query->where('a.lesson_id',$lesson->id)->orderBy("a.order","asc")->get();

                    $lessons[$key2] = $lesson;
                }

                $level->lessons = $lessons;
                $levels[$key] = $level;
            }

            

            
           
            $porcen_course = $test_f > 0 ? ($test_f*100/$num_test) : 0;

            return view('public.courses.showDetails',compact(array('course','levels','info_activity', 'my_course_info', 'activity_id','porcen_course')) ); 
        }
        return redirect()->back();
        
    }

    public function showTest(){

                 
        $activity_id = request("id_activity");
        $course_id = request("course_id");

        if(!Auth::check()){
            return response()->json(array("error"=>true, "msg"=>"Es necesario esta registrado para ver evaluación"));
        }
        $id_user = Auth::user()->id;
        $my_course_info = DB::table("courses_students")->where('course_id',$course_id)->where('user_id',$id_user)->where('deleted',0)->first();

        $info_activity = Activity::getInfoActivity($activity_id);

        if(empty($my_course_info->course_id)){
            return response()->json(array("error"=>true, "msg"=>"Es necesario estar registrado en el curso"));
        }

        $questions = DB::table("test_questions as tq")
        ->select(["q.questions","q.answer_id","id","q.type"])
        ->join("questions AS q","q.id" ,"=", "tq.question_id")
        ->where("tq.deleted",0)->where("q.deleted",0)->where("tq.activity_id",$activity_id)->limit($info_activity->number_questions)->get()->toArray();
        
        foreach ($questions as $key => $question) {
            $questions[$key]->options=
            Option::select(["enunciate","id"])->where("deleted",0)->where("question_id",$question->id)->inRandomOrder()->get(); 
        }

        $html = view::make('public.courses.showTest',compact(array('questions','info_activity','my_course_info','activity_id','course_id')));
        return response()->json(array('error'=>false,'msg'=>'','html' => $html->render()));
    }

    public function setRegisterCourse($course_id){

        try {
            
            $msg = array("error"=>true, "msg"=>'Curso no encontrado');
            $course_info = DB::table("courses")->where('id',$course_id)->where('deleted',0)->first();

            if(Auth::guest()){
                $msg = array("error"=>true, "msg"=>'Es necesario iniciar sessión para acceder a este curso');
            
            }
            else if(!empty($course_info)){

                $id_user = Auth::user()->id;
                $course = DB::table("courses_students")->where('course_id',$course_id)->where('user_id',$id_user)->where('deleted',0)->first();
                
                $msg = array("error"=>true, "msg"=>'Ya estas registrado en este curso');

                if(empty($course->id)){
                
                    $student = new Courses_student;

                    $student->course_id = $course_id;
                    $student->user_id = $id_user;
                    $student->state = 1;
                    $student->purchase_price = $course_info->is_free != 1? $course_info->price : 0;

                    $student->save();

                    $msg = array("error"=>false, "msg"=>'Registro exitoso');
                }

            }
        } catch (\Throwable $th) {
            $msg = array("error"=>true, "msg"=>'No fue posible fininalizar el registro'.$th);
        }
        

        return response()->json($msg, 200);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveTest($course_id, $activity_id)
    {

        $id_user = Auth::user()->id;
        $info_activity = Activity::getInfoActivity($activity_id);
        $response = [];
        $num_correct = 0;
        $is_certificate = false;
        $num_question = count(request('questions'));

        $questions_order = json_decode(request('questions_order'));

        foreach ($questions_order as $key => $value) {
            $question = TestQuestion::getInfoTest($value->id, $activity_id, 2);
            $num_correct++;
            $result = array('id' =>$value->id, 'is_check' => true);
            foreach ($question as $key => $item) {
                
                if($item->option_id != $value->option[$key]){
                    $num_correct--;
                    $result = array('id' => $value->id, 'is_check' => false);

                    break;
                }
            }

            $response[] = $result;
        }

        $num_question += count($questions_order);

        // realizamos las verificacion del numero de preguntas correctas
        foreach (request('questions') as $key => $value) {

            $question = TestQuestion::getInfoTest($key, $activity_id);

            if($question->answer_id == $value)
                $num_correct++;
            
            $response[] = array('id' => $value, 'is_check' => ($question->answer_id == $value));
        }
        $data["response"] = $response;
        $porcent_q = round(($num_correct*100) / $num_question, 2);

        if($porcent_q >= $info_activity->porcent_acceptance){

            // actualizamos la tabla de actividades vistas con calificacion y numero de intentos

            $performed = Performed_activitie::where('activity_id',$activity_id)->where('user_id',$id_user)->first();

            if(!$performed){
                $performed = new Performed_activitie;
                $new_porcent_q = ($performed->porcent_q < $porcent_q);

                $performed->user_id = $id_user;
                $performed->activity_id = $activity_id;
                $performed->show = 1;
                
                if($new_porcent_q){
                    $performed->porcent_q = $porcent_q;
                    $performed->num_questions = $num_question;
                    
                }
                $performed->course_id = $course_id;
                $performed->save();
            }else{
                DB::table('performed_activities')->where('activity_id',$activity_id)->where('user_id',$id_user)
                    ->update(['porcent_q' => $porcent_q, 'num_questions' => $num_question,]);
            }

            // varificamos si la nueva calificacion es mas alta que la anterior
            

            // realizamos una consulta para verificar si los test ya fueron finalizados
            $num_test = Performed_activitie::getQuantityTest($course_id);
            
            $test_f = Performed_activitie::getQuantityTestStudent($course_id, $id_user); 

            if($num_test == $test_f){
                // damos por finalizado un curso
                $is_certificate = DB::table('courses_students')->where('course_id',$course_id)->where('user_id',$id_user)
                    ->update(['approved' => 1, 'state' => 0, 'end_date' => date("Y-m-d H:i:s")]);
            }

        }
        
        $data["is_certificate"] = $is_certificate;
        $data["porcent_q"] = $porcent_q;
        return response()->json($data, 200);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        //
    }
}
