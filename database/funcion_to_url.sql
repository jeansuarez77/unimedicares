
DROP FUNCTION IF EXISTS `to_url`;

DELIMITER $$
CREATE FUNCTION `to_url` (title VARCHAR(255), id VARCHAR(255))
RETURNS VARCHAR(255)
BEGIN
 DECLARE salida VARCHAR(255);
  SET salida = lower(REGEXP_REPLACE(REPLACE(TRIM(title),'áéíóúÁÉÍÓÚäëïöüÄËÏÖÜñÑ!"#$%&*+,./:;<=>?@[\]^`{|}~.','aeiouAEIOUaeiouAEIOUnN '), "[^[:alpha:]^[:alnum:]]", '-'));
  RETURN CONCAT(salida,"-",id);
END$$

DELIMITER ;