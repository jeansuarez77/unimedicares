<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas', function (Blueprint $table) {
            $table->id();
            $table->string("name",180);
            $table->text("description")->nullable();
            $table->text("img_url")->nullable();
            $table->tinyInteger('deleted')->default("0")->index();
            $table->timestamps();
        });
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('area_id')->unsigned();
            $table->string("name",180)->index();
            $table->integer("intensity");
            $table->string("code",20)->index();
            $table->string("url",190)->nullable()->unique();
            $table->text("description")->nullable();
            $table->text("img_vide")->nullable();
            $table->string("logo",150)->nullable();
            $table->double("validity")->default("30")->nullable();
            
            $table->double("price")->nullable();
            $table->tinyInteger('deleted')->default("0")->index();
            $table->tinyInteger('is_free')->default("1")->index();            
            $table->foreign('area_id')->references('id')->on('areas');

            $table->timestamps();
        });

        Schema::create('levels', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('course_id')->unsigned();
            $table->text("description")->nullable();
            $table->tinyInteger('deleted')->default("0")->index();
            $table->integer("order")->index();
            $table->foreign('course_id')->references('id')->on('courses');
            $table->timestamps();
        });
        Schema::create('lessons', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('level_id')->unsigned();
            $table->text("description")->nullable();
            $table->tinyInteger('deleted')->default("0")->index();
            $table->integer("order")->index();
            $table->timestamps();
            $table->foreign('level_id')->references('id')->on('levels');

        });
        Schema::create('activities', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('lesson_id')->unsigned();
            $table->string("name",180)->nullable();
            $table->integer("type")->comment("1-vide, 2-infografia, 3-prueba")->default(1);
            $table->text("file_url")->nullable();
            $table->integer("trial_numbers")->nullable();
            $table->integer("order")->index();
            $table->text("description")->nullable();
            $table->integer('number_questions')->nullable();
            $table->tinyInteger('deleted')->default("0")->index();
            $table->double('porcent_acceptance')->default("80")->index();            
            $table->timestamps();
            $table->foreign('lesson_id')->references('id')->on('lessons');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
