<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTypeDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('types_documents', function (Blueprint $table) {
            $table->id();
            $table->string('code',20)->nullable()->unique();
            $table->string("name",100);
            $table->tinyInteger('deleted')->index();            
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('document_type_id')->references('id')->on('types_documents');
                      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('types_documents');
    }
}
