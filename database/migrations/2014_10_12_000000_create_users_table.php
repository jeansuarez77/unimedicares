<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',150);
            $table->string('surname',100);
            $table->string('second_surname',100);
            $table->string('email',180)->unique();
            $table->string('document',20)->nullable();
            $table->bigInteger('document_type_id')->nullable()->unsigned();
            $table->string('password',180);
            $table->string('rol',50)->default("user");
            $table->string('country_code',6)->default("CO")->nullable();;
            $table->bigInteger('department_id')->nullable()->unsigned();
            $table->bigInteger('city_id')->nullable()->unsigned();
            $table->string('address',150)->nullable();
            $table->string('phone',30)->nullable();
            $table->date('date_birth')->nullable();
            $table->tinyInteger('deleted')->default("0")->index();
            $table->timestamp('email_verified_at')->nullable();
            $table->bigInteger('document_city_id')->nullable()->unsigned();            
            $table->rememberToken();
            $table->timestamps();
           
            $table->unique(['document_type_id', 'document']);

            $table->foreign('country_code')->references('code')->on('countries');
            $table->foreign('department_id')->references('id')->on('departments');
            $table->foreign('city_id')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
