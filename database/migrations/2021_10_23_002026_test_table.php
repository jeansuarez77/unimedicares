<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->string("questions",180)->index();
            $table->bigInteger('answer_id')->unsigned()->nullable();
            $table->bigInteger('created_by')->unsigned(); 
            $table->bigInteger('area_id')->unsigned();
            $table->tinyInteger('type')->default("1")->index();                 

            $table->tinyInteger('deleted')->default("0")->index();
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('area_id')->references('id')->on('areas');     
        });
        Schema::create('options', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('question_id')->unsigned();
            $table->string("enunciate",180)->index();
            $table->integer('order')->nullable();
            $table->tinyInteger('deleted')->default("0")->index();
            $table->foreign('question_id')->references('id')->on('questions');

        });

        Schema::create('test_questions', function (Blueprint $table) {
            $table->bigInteger('question_id')->unsigned();
            $table->bigInteger('activity_id')->unsigned();
            $table->tinyInteger('deleted')->default("0")->index();
            $table->foreign('question_id')->references('id')->on('questions');
            $table->foreign('activity_id')->references('id')->on('activities');
            $table->primary(["question_id","activity_id",]);
        });

        Schema::table('questions', function (Blueprint $table) {
           // $table->foreign('answer_id')->references('id')->on('options');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
