<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesStudents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses_students', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('course_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->tinyInteger('state');
            $table->tinyInteger('approved')->nullable();
            $table->dateTime("end_date")->nullable();
            $table->double('purchase_price')->index()->default(0);
            $table->double('progress')->index()->default(0);
            $table->double("validity")->default("30")->nullable();
            $table->tinyInteger('deleted')->index()->default(0);
            $table->text("transactionId")->nullable();
            $table->text("transactionState")->nullable();
            $table->dateTime("date_transaction")->nullable();            
            $table->tinyInteger('is_payment_finished')->index()->default(0);            
            $table->timestamps();
            $table->unique(['course_id', 'user_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses_students');
    }
}
