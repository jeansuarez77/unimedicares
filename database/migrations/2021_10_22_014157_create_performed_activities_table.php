<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerformedActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performed_activities', function (Blueprint $table) {

            $table->bigInteger('activity_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->tinyInteger('show')->default(1);
            $table->double("porcent_q")->nullable();
            $table->integer("num_questions")->nullable();
            $table->integer("trial_numbers")->nullable();
            $table->tinyInteger('deleted')->default(0);
            $table->bigInteger('course_id')->unsigned();

            $table->foreign('activity_id')->references('id')->on('activities');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('course_id')->references('id')->on('courses');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performed_activities');
    }
}
