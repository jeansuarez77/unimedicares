<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {            
            $table->string('code',5)->primary();
            $table->string('name');
            $table->tinyInteger('deleted')->index()->default(0);            
        });
        Schema::create('departments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('country_code',5);
            $table->string('name',180);
            $table->tinyInteger('deleted')->index()->default(0);
            $table->foreign('country_code')->references('code')->on('countries');
        });
        Schema::create('cities', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->bigInteger('department_id')->unsigned();
            $table->string('country_code',5);
            $table->string('name',180);
            $table->string('cod_postal',20)->nullable()->index();
            $table->tinyInteger('deleted')->index()->default(0);    
            $table->foreign('department_id')->references('id')->on('departments');
        
            $table->foreign('country_code')->references('code')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
        Schema::dropIfExists('departments');
        Schema::dropIfExists('countries');       
    }
}
