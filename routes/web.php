<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\CoursesController;
use App\Http\Controllers\Admin\StudentsController;
use App\Http\Controllers\Services\ProfileController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\Admin\QuestionBanksController;
use App\Http\Controllers\UsersBanksController;
use App\Http\Controllers\PayController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();



//------ public ----
Route::group(['prefix' => 'programas'], function () {   
    Route::post('curso/save_test/{course_id}/{activity_id}',  [App\Http\Controllers\Services\CoursesController::class,'saveTest'])->name("course.save_test");
    Route::get('',  [App\Http\Controllers\Services\CoursesController::class ,'index'])->name('courses.index');
    Route::get('curso/show_tets',  [App\Http\Controllers\Services\CoursesController::class,'showTest']);
    Route::get('courses_areas/{area}',  [App\Http\Controllers\Services\CoursesController::class ,'get_courses_area']);
    Route::get('curso/registrar/{id}',  [App\Http\Controllers\Services\CoursesController::class,'setRegisterCourse'])->name("course.start");
    Route::get('curso/{url}/{activity_id?}',  [App\Http\Controllers\Services\CoursesController::class ,'getCourseDetails'])->name('courses.course');
   
});

//------ public ----


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/not_authorized', [App\Http\Controllers\HomeController::class, 'not_authorized'])->name('home');


Route::group([
    'middleware' => ['auth'],
    'prefix' => 'admin/students'], function () {   
    Route::get('/',  [StudentsController::class ,'index']);
});
Route::group([
    'middleware' => ['auth'],
    'prefix' => 'students'], function () {   
    Route::get('certificate/{student_id}/{course_id}',  [StudentsController::class ,'generate'])->name('students.certificate');
    Route::get('profile',  [StudentsController::class ,'profile'])->name('students.profile');
    Route::post('change-password',  [UsersController::class ,'change_password'])->name('students.change-password');
    Route::post('update-info',  [UsersController::class ,'update_info'])->name('students.update-info');
    Route::get('checkouts/{id}',  [PayController::class ,'pay_course'])->name('students.pay');
    Route::get('transaction',  [PayController::class ,'processTransaction'])->name('transaction.pay');
    
    
});
Route::group([
    'middleware' => ['auth',"checkAdmin"],
    'prefix' => 'admin/courses'], function () {   
    Route::get('/',  [CoursesController::class ,'index']);
    Route::get('areas',  [CoursesController::class ,'areas']);
    Route::post('create-area/{id?}',  [CoursesController::class ,'create_area'])->name("courses.create-area");
    Route::get('get-area/{id}',  [CoursesController::class ,'get_area'])->name("courses.get-area");
    Route::delete('delete-area/{id}',  [CoursesController::class ,'delete_area'])->name("courses.delete-area");
    Route::get('courses-view/{id?}',  [CoursesController::class ,'courses_view'])->name("courses.courses-view");
    Route::post('save-course/{id?}',  [CoursesController::class ,'save_course'])->name("courses.save-course");
    Route::delete('delete-course/{id}',  [CoursesController::class ,'delete_course'])->name("courses.delete-course");
    Route::get('courses-manage/{id}',  [CoursesController::class ,'courses_manage'])->name("courses.courses-manage");
    Route::get('get-levels/{id}',  [CoursesController::class ,'get_levels'])->name("courses.get-levels");
    Route::post('create-level/{id}',  [CoursesController::class ,'create_level'])->name("courses.create-level");
    Route::post('create-lesson/{id}',  [CoursesController::class ,'create_lesson'])->name("courses.create-lesson");
    Route::post('create-activity/{id?}',  [CoursesController::class ,'create_activity'])->name("courses.create-activity");
    Route::get('get-activity/{activity_id}/{lesson_id}',  [CoursesController::class ,'get_activity'])->name("courses.get-activity");
    Route::post('delete-activity/{activity_id}/{lesson_id}',  [CoursesController::class ,'delete_activity'])->name("courses.delete-activity");    
    Route::post('delete-question/{question_id}/{activity_id}',  [CoursesController::class ,'delete_question'])->name("courses.delete-question");
    Route::get('get-test-questions/{activity_id}',  [CoursesController::class ,'get_test_questions'])->name("courses.get-test-questions");
    Route::post('delete-leve/{id}',  [CoursesController::class ,'delete_leve'])->name("courses.delete-leve");
    Route::post('delete-lesson/{id}',  [CoursesController::class ,'delete_lesson'])->name("courses.delete-lesson");
    Route::post('add-question',  [CoursesController::class ,'add_question'])->name("courses.add-question");

});
Route::group([
    'middleware' => ['auth',"checkAdmin"],
    'prefix' => 'admin/questions'], function () { 
    
    Route::get('/',  [QuestionBanksController::class ,'index']);
    Route::get('question-view/{id?}',  [QuestionBanksController::class ,'view'])->name("question.view");
    Route::post('create-question/{id?}',  [QuestionBanksController::class ,'create_question'])->name("question.create-question");
    Route::get('get-question/{id}',  [QuestionBanksController::class ,'get_question'])->name("question.get-question");
    Route::get('get-areas',  [QuestionBanksController::class ,'get_areas'])->name("question.get-areas");
    Route::post('delete-question/{question_id}',  [QuestionBanksController::class ,'delete_question'])->name("question.delete-question");
    Route::get('searcher-question',  [QuestionBanksController::class ,'searcher_question'])->name("question.searcher-question");
    Route::get('get-option/{id}',  [QuestionBanksController::class ,'get_option'])->name("question.get-option");
    
});

Route::group([
    'middleware' => ['auth',"checkAdmin"],
    'prefix' => 'admin/users'], function () {     
    Route::get('/',  [UsersController::class ,'list']);    
    Route::post('create-user/{id?}',  [UsersController::class ,'create_user'])->name("users.create-user");
    Route::get('get-user/{id}',  [UsersController::class ,'get_user'])->name("users.get-user"); 
    
    Route::post('delete-user/{id}',  [UsersController::class ,'delete_user'])->name("users.delete-user");

});

Route::get('/offline', function () {    
    return view('vendor/laravelpwa/offline');
});
